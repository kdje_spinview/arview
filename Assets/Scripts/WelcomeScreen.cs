﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WelcomeScreen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AnimationEnd()
    {
        this.gameObject.SetActive(false);
        Main.Instance.guiController.Mode = AppMode.IDLE;
    }
}
