﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//only windows

#if UNITY_WSA
public class MainTrackableEventHandler : DefaultTrackableEventHandler
{
    static bool _tracking = false;
    public static bool Tracking
    {
        get
        {
            return _tracking;
        }
        private set
        {
            _tracking = value;

            if (InteractionController.Instance.objectToPlace != null)
                InteractionController.Instance.objectToPlace.SetActive(_tracking);

            //if (!Info.Shown)
            //    Main.Instance.ShowInfo("no_marker");
        }
    }
    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();
        Tracking = true;
    }

    protected override void OnTrackingLost()
    {
        base.OnTrackingFound();
        Tracking = false;
    }
}
#endif