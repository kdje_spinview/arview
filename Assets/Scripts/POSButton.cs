﻿using Lean.Touch;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class POSButton : MonoBehaviour {

    public Image image;
    public Text posName;

    public void SelectPOS()
    {
        /*if (InteractionController.Instance.objectToPlace != null)
        {
            Destroy(InteractionController.Instance.objectToPlace);
        }
        foreach (Transform child in Main.Instance.objRoot.transform)
        {
            GameObject.Destroy(child.gameObject);
        }*/
        Main.Instance.ProductPlaced = false;
        POSFileManager.Instance.SelectPOS(posName.text);
        //InteractionController.Instance.objectToPlace = Instantiate(Main.Instance.models[Convert.ToInt32(posName.text) - 1], Main.Instance.objRoot.transform);
        //InteractionController.Instance.objectToPlace = Main.Instance.models[Convert.ToInt32(posName.text) - 1];
        //InteractionController.Instance.objectToPlace.AddComponent<LeanTranslate>();
        //InteractionController.Instance.objectToPlace.AddComponent<LeanScale>();
        //InteractionController.Instance.objectToPlace.AddComponent<LeanRotate>();
        //InteractionController.Instance.objectToPlace.transform.localScale = new Vector3(0.007f, 0.007f, 0.007f);
        //InteractionController.Instance.objectToPlace.transform.eulerAngles = new Vector3(0, 180, 0);
        //InteractionController.Instance.placementReady = true;
        //InteractionController.Instance.placementIndicator.SetActive(true);

        Main.Instance.PosCatalogueButton();
    }

    public void SetButton(string name, Sprite sprite = null)
    {
        posName.text = name;
        if (sprite != null)
        {
            image.sprite = sprite;
            image.gameObject.SetActive(true);
        }
        else posName.gameObject.SetActive(true);
    }
}
