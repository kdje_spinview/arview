﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using Lean.Touch;

public enum AppMode
{
    IDLE,
    MODEL
}

public class GuiController : MonoBehaviour
{
    public Transform CaptureButtonPortrait;
    public Transform UndoButtonPortrait;
    public Transform ModelsButtonPortrait;
    public Transform CaptureButtonPortraitRoot;
    public Transform UndoButtonPortraitRoot;
    public Transform ModelsButtonPortraitRoot;

    public Transform ModelOkButtonPortrait;
    public Transform LockScaleButtonPortrait;
    public Transform ResetButtonPortrait;
    public Transform ModelOkButtonPortraitRoot;
    public Transform LockScaleButtonPortraitRoot;
    public Transform ResetButtonPortraitRoot;
    //portrait orientation//public Transform CaptureButtonLandscape;
    //public Transform UndoButtonLandscape;
    //portrait orientation//public Transform ModelsButtonLandscape;


    public Transform SettingsButton;
    public GameObject SettingsUpdateNotification;
    
    public Button ModelsButton;
    public Button CaptureButton;
    public Button RulerButton;

    public Button LockButton;
    public Button ModelOkButton;
    public Button ResetButton;

    public Image ScaleButtonImage;
    public Image RulerButtonImage;

    public Button OpenLibraryButton;

    public Button DiscardButton;

    private void Awake()
    {
        ModelsButton.onClick.AddListener(Main.Instance.PosCatalogueButton);
        CaptureButton.onClick.AddListener(Main.Instance.ScreenShotButton);
        RulerButton.onClick.AddListener(Main.Instance.ActivateRulerClicked);

        LockButton.onClick.AddListener(Main.Instance.LockClicked);
        ModelOkButton.onClick.AddListener(Main.Instance.ModelOkClicked);
        ResetButton.onClick.AddListener(Main.Instance.UndoButton);

        FixButtonsSize();

        //Mode = AppMode.IDLE;

        OpenLibraryButton.onClick.AddListener(() => { Debug.Log("OpenLibraryButton clicked"); PopupManager.Instance.OpenPopup(PopupType.LIBRARY); });

        DiscardButton.onClick.AddListener(() => InteractionController.Instance.DestroyPlacedObject(true));
    }
#if !UNITY_ANDROID
    private void Start()
    {
        Mode = AppMode.IDLE;
    }
#endif
    void FixButtonsSize()
    {
        DeviceType deviceType = Util.GetDevice();
        //portrait orientation//Orientation orientation = Util.GetOrientation();

        float _smaller = Util.ScreenSmallerSize;
        float _bigger = Util.ScreenBiggerSize;
        switch (deviceType)
        {
            case DeviceType.MOBILE:
                if (1080f < _smaller)
                {
                    CaptureButtonPortrait.transform.localScale =
                    UndoButtonPortrait.transform.localScale =
                    ModelsButtonPortrait.transform.localScale = Vector3.one * 1080f / _smaller;

                    CaptureButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition *= 1080f / _smaller;
                    UndoButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition *= 1080f / _smaller;
                    ModelsButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition *= 1080f / _smaller;


                    ModelOkButtonPortrait.transform.localScale =
                    LockScaleButtonPortrait.transform.localScale =
                    ResetButtonPortrait.transform.localScale = Vector3.one * 1080f / _smaller;

                    ModelOkButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition *= 1080f / _smaller;
                    LockScaleButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition *= 1080f / _smaller;
                    ResetButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition *= 1080f / _smaller;
                }
                break;
            case DeviceType.TABLET:
                CaptureButtonPortrait.transform.localScale = 
                UndoButtonPortrait.transform.localScale = 
                ModelsButtonPortrait.transform.localScale =

                ModelOkButtonPortrait.transform.localScale =
                LockScaleButtonPortrait.transform.localScale =
                ResetButtonPortrait.transform.localScale =

                SettingsButton.transform.localScale = Vector3.one / 2;

                CaptureButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition /= 2;
                UndoButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition /= 2;
                ModelsButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition /= 2;

                ModelOkButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition /= 2;
                LockScaleButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition /= 2;
                ResetButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition /= 2;

                break;
        }
    }

    public void SetModelsButton(bool active)
    {
        ModelsButtonPortrait.gameObject.SetActive(active);
        //portrait orientation//ModelsButtonLandscape.gameObject.SetActive(active);
    }


#region mode_changing
    public Animator _animatorIdle;
    public Animator _animatorModel;
    public Animator _TopAnimatorIdle;
    public Animator _TopAnimatorModel;

    AppMode _mode;
    public AppMode Mode
    {
        get
        {
            return _mode;
        }
        set
        {
            _mode = value;

            switch (_mode)
            {
                case AppMode.IDLE:
                    if (_animatorIdle.enabled) _animatorIdle.Play("IdleUiEnter");
                    else _animatorIdle.enabled = true;

                    if (_animatorModel.enabled) _animatorModel.Play("ModelUiExit");
                    //else _animatorModel.enabled = true;


                    if (_TopAnimatorIdle.enabled) _TopAnimatorIdle.Play("TopIdleUiEnter");
                    else _TopAnimatorIdle.enabled = true;

                    if (_TopAnimatorModel.enabled) _TopAnimatorModel.Play("TopModelUiExit");
                    //else _TopAnimatorModel.enabled = true;

                    //Main.Instance.guiController.DiscardButton.gameObject.SetActive(false);
                    InteractionController.Instance.DisableLeanComponents();
                    break;
                case AppMode.MODEL:
                    if (_animatorModel.enabled) _animatorModel.Play("ModelUiEnter");
                    else _animatorModel.enabled = true;

                    if (_animatorIdle.enabled) _animatorIdle.Play("IdleUiExit");
                    //else _animatorIdle.enabled = true;

                    if (_TopAnimatorModel.enabled) _TopAnimatorModel.Play("TopModelUiEnter");
                    else _TopAnimatorModel.enabled = true;

                    if (_TopAnimatorIdle.enabled) _TopAnimatorIdle.Play("TopIdleUiExit");
                    //else _TopAnimatorIdle.enabled = true;

                    //Main.Instance.guiController.DiscardButton.gameObject.SetActive(true);
                    InteractionController.Instance.
                        EnableLeanComponents(Prefs.SizeLock);
                    break;
            }
        }
    }
    /*TESTING:
     * IEnumerator State()
    {
        int i = 0;
        while (true)
        {
            yield return new WaitForSeconds(3f);


            Mode = (i++ % 2 != 0 ? AppMode.MODEL : AppMode.IDLE); 
        }
    }
    private void Start()
    {
        StartCoroutine(State());
    }
    TESTING*/
#endregion
}
