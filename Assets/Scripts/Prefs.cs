﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public static class Prefs {

    static string JsonDataPrefs = "JsonData";


    public static string GetJsonDataPref()
    {
        return GetString(JsonDataPrefs);
    }

    static public void SetJsonDataPref(string value)
    {
        SetString(JsonDataPrefs, value);
    }

    static void SetString(string key, string value)
    {
        PlayerPrefs.SetString(key, value);
    }

    static string GetString(string key, string defaultValue = "")
    {
        return PlayerPrefs.GetString(key, defaultValue);
    }


    #region log_in
    const string LOGINDATA = "logindata";
    const string USERNAMEKEY = "username";
    const string PASSKEY = "passwrod";
    const string COMPANYKEY = "company";
    const string DISPLAYNAMEKEY = "displayname";
    const string EMAILKEY = "email";
    const string IMAGEKEY = "imagekey";
    const string COMPANYIMAGEKEY = "companyimagekey";
    const string FOLDERID = "folderID";
    const string COMPANYID = "companyID";
    const string ACCESSTOKEN = "accessToken";

    static Dictionary<string, string> _cachedLogInData = null;
    public static void SetLoginData(string username, string password, string company, string displayName, string email, string imageKey, string companyImageKey, string folderID, string companyID, string accessToken)
    {
        Debug.Log("LogIn setLogInData + " + username + password);
        Debug.Log("login __cachedLogInData = new Dictionary<string, string>(");
        _cachedLogInData = new Dictionary<string, string>() { { USERNAMEKEY, username }, { PASSKEY, password }, { COMPANYKEY, company }, { DISPLAYNAMEKEY, displayName }, { EMAILKEY, email }, { IMAGEKEY, imageKey }, { COMPANYIMAGEKEY, companyImageKey }, { FOLDERID, folderID }, { COMPANYID, companyID }, { ACCESSTOKEN, accessToken } };
        string logInDataString = JsonConvert.SerializeObject(_cachedLogInData);
        PlayerPrefs.SetString(LOGINDATA, logInDataString);
    }
    public static void ResetLoginData()
    {
        _cachedLogInData = null;
        PlayerPrefs.SetString(LOGINDATA, null);
    }
    public static Dictionary<string, string> GetLogInData()
    {
        Debug.Log("LogIn  (_cachedLogInData != null) " + (_cachedLogInData != null));
        if (_cachedLogInData != null) return _cachedLogInData;
        string logInDataString = PlayerPrefs.GetString(LOGINDATA);
        Debug.Log("LogIn  (logInDataString != null) " + (string.IsNullOrEmpty(logInDataString)));

        Debug.Log("LogIn  (logInDataString =) " + logInDataString);
        if (string.IsNullOrEmpty(logInDataString)) return null;

        try
        {
            Debug.Log("LogIn  try login");
            Dictionary<string, string> logInData = JsonConvert.DeserializeObject<Dictionary<string, string>>(logInDataString);
            if (string.IsNullOrEmpty(logInData[USERNAMEKEY]) || string.IsNullOrEmpty(logInData[PASSKEY]) || string.IsNullOrEmpty(logInData[COMPANYKEY])/* || string.IsNullOrEmpty(logInData[DISPLAYNAMEKEY]) || string.IsNullOrEmpty(logInData[EMAILKEY])*/)
                return null;
            Debug.Log("login _cachedLogInData = logInData;");
            _cachedLogInData = logInData;
            return logInData;
        }
        catch
        {
            return null;
        }
    }
    public static string GetCompanyName()
    {
        if (_cachedLogInData == null) GetLogInData();
        if (_cachedLogInData == null) return null;
        return _cachedLogInData.ContainsKey(COMPANYKEY) ? _cachedLogInData[COMPANYKEY] : "";
    }
    public static string GetDisplayName()
    {
        if (_cachedLogInData == null) GetLogInData();
        if (_cachedLogInData == null) return null;
        return _cachedLogInData.ContainsKey(DISPLAYNAMEKEY) ? _cachedLogInData[DISPLAYNAMEKEY] : "";
    }
    public static string GetEMail()
    {
        if (_cachedLogInData == null) GetLogInData();
        if (_cachedLogInData == null) return null;
        return _cachedLogInData.ContainsKey(EMAILKEY) ? _cachedLogInData[EMAILKEY] : "";
    }
    public static string GetUsername()
    {
        if (_cachedLogInData == null) GetLogInData();
        if (_cachedLogInData == null) return null;
        return _cachedLogInData.ContainsKey(USERNAMEKEY) ? _cachedLogInData[USERNAMEKEY] : "";
    }
    public static string GetPassword()
    {
        if (_cachedLogInData == null) GetLogInData();
        if (_cachedLogInData == null) return null;
        return _cachedLogInData.ContainsKey(PASSKEY) ? _cachedLogInData[PASSKEY] : "";
    }
    public static string GetImageKey()
    {
        if (_cachedLogInData == null) GetLogInData();
        if (_cachedLogInData == null) return null;
        return _cachedLogInData.ContainsKey(IMAGEKEY) ? _cachedLogInData[IMAGEKEY] : "";
    }
    public static string GetCompanyImageKey()
    {
        if (_cachedLogInData == null) GetLogInData();
        if (_cachedLogInData == null) return null;
        return _cachedLogInData.ContainsKey(COMPANYIMAGEKEY) ? _cachedLogInData[COMPANYIMAGEKEY] : "";
    }
    public static string GetFolderID()
    {
        if (_cachedLogInData == null) GetLogInData();
        if (_cachedLogInData == null) return null;
        return _cachedLogInData.ContainsKey(FOLDERID) ? _cachedLogInData[FOLDERID] : "";
    }
    public static string GetCompanyID()
    {
        if (_cachedLogInData == null) GetLogInData();
        if (_cachedLogInData == null) return null;
        return _cachedLogInData.ContainsKey(COMPANYID) ? _cachedLogInData[COMPANYID] : "";
    }
    public static string GetAccessToken()
    {
        if (_cachedLogInData == null) GetLogInData();
        if (_cachedLogInData == null) return null;
        return _cachedLogInData.ContainsKey(ACCESSTOKEN) ? _cachedLogInData[ACCESSTOKEN] : "";
    }
    #endregion

    #region settings
    const string LANGUAGEKEY = "language";
    public static Language Language
    {
        get
        {
            int lang = PlayerPrefs.GetInt(LANGUAGEKEY, 0);
            return (Language)lang;
        }
        set
        {
            PlayerPrefs.SetInt(LANGUAGEKEY, (int)value);
        }
    }
    const string SIZELOCKKEY = "sizelock";
    public static bool SizeLock
    {
        get
        {
            return PlayerPrefs.GetInt(SIZELOCKKEY, 1) == 0 ? false : true;
        }
        set
        {
            PlayerPrefs.SetInt(SIZELOCKKEY, value? 1 : 0);
        }
    }
    #endregion

    #region ruler

    const string RULERKEY = "ruler";
    public static bool RulerAcive
    {
        get
        {
            return PlayerPrefs.GetInt(RULERKEY, 1) == 0 ? false : true;
        }
        set
        {
            PlayerPrefs.SetInt(RULERKEY, value ? 1 : 0);
        }
    }
    #endregion

}
