using Dummiesman;
using Lean.Touch;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using TriLib;
using UnityEngine;
using UnityEngine.Networking;


public class POSFileManager : MonoBehaviour
{

    #region SINGLETON
    private static POSFileManager instance = null;

    public static POSFileManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<POSFileManager>();
            }

            return instance;
        }
    }
    #endregion

    AssetBundle currentAssetBundle;
    public ModelData model;
    //public GameObject imageTarget;
    //public GameObject currentPOS;

    // Update is called once per frame
    void Start () {
        //InstantiateModel(Application.dataPath + "/Resources/Models/BretonBox.obj");
        //InstantiateModel(Application.dataPath + "/BretonBox/v_3cb_1sjcagad1/BretonBox.obj");
    }

    void Test(string path = "")
    {
        //path = Application.dataPath + "/BretonBox/v_3cb_1sjcagad1/BretonBox.obj";
        //InstantiateModel(path);
    }

    public void SelectPOS(string name)
    {
        // _currentBrand = brandName;
        print("JOKAN SelectPOS");
        InteractionController.Instance.DestroyPlacedObject();

        Info.ModelSelected = true;
#if !UNITY_WSA
        InteractionController.Instance.SetPlane(true);
#endif
        StartCoroutine(LoadPOS(name));
        //StartCoroutine(currentAssetBundle != null ? UnloadBundle(name) : LoadPOS(name));
    }
    /*private IEnumerator UnloadBundle(string name)
    {
        currentAssetBundle.Unload(false);
        yield return new WaitForSeconds(0.5f);
        currentAssetBundle = null;
        Destroy(InteractionController.Instance.objectToPlace);
        InteractionController.Instance.objectToPlace = null;
        StartCoroutine(LoadPOS(name));
    }*/

    private IEnumerator Download(string url, string name, string type)
    {
        int token = WebRequest.GetNewToken();
        yield return WebRequest.Get(token, url);
        if (!WebRequest.Error(token))
        {
            string savePath = string.Format("{0}/{1}" + type, Application.persistentDataPath + "/" + name, name);
            File.WriteAllBytes(savePath, WebRequest.BytesData(token));
            print("JOKAN Download success = " + name);
        }
        else
        {
            print("JOKAN Download failed = " + name);
        }
        /*WWW www = new WWW(url);
        Main.Instance.loading.StartLoading(www);
        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            string savePath = string.Format("{0}/{1}" + type, Application.persistentDataPath + "/" + name, name);
            File.WriteAllBytes(savePath, www.bytes);
            print("JOKAN Download success = " + name);
        }
        else
        {
            print("JOKAN Download failed = " + name);
        }*/
    }

    private IEnumerator LoadPOS(string name)
    {
        string path = Application.persistentDataPath + "/" + name;
        model = Settings.PlatformModels[name];

        Axis.is3dModel = model.type == Settings.ModelType.Model;

        Directory.CreateDirectory(path);
        bool stored = IsModelStored(path);
        if (!stored || !Settings.StoredModels.ContainsKey(name) || Convert.ToDateTime(Settings.StoredModels[name].lastUpdateDate) < Convert.ToDateTime(model.lastUpdateDate))
        {
            if (stored)
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(path);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
            }

            string link = Settings.GetPrepareDownloadLink(model.id);
            byte[] myData = System.Text.Encoding.UTF8.GetBytes("This is some test data");
            UnityWebRequest www = UnityWebRequest.Put(link, myData);
            www.chunkedTransfer = false;
            Debug.Log("JOKAN Prepare Download");
            yield return www.SendWebRequest();
            
            if (string.IsNullOrEmpty(www.error))
            {
                Debug.Log(www.downloadHandler.text);

                Dictionary<string, object> response = JsonConvert.DeserializeObject<Dictionary<string, object>>(www.downloadHandler.text);
                if (response.ContainsKey("id"))
                {
                    www = UnityWebRequest.Get(Settings.GetDownloadLink(response["id"].ToString()));
                    www.chunkedTransfer = false;
                    Main.Instance.loading.StartLoading(www);
                    Debug.Log("JOKAN Download");
                    yield return www.SendWebRequest();
                    
                    if (string.IsNullOrEmpty(www.error))
                    {
                        Debug.Log("JOKAN Downloaded");
                        string zipPath = path + "/" + name + ".zip"; ;
                        string unzipPath = path;
                        var data = www.downloadHandler.data;
                        File.WriteAllBytes(zipPath, data);
                        Unzip(zipPath, unzipPath);                        
                        SaveModelData(name);
                    }
                    else
                    {
                        Main.Instance.Notify(Settings.SOMETHING_WENT_WRONG);
                        yield break;
                    }
                }
            }
            else
            {
                Main.Instance.Notify(Settings.SOMETHING_WENT_WRONG);
                yield break;
            }
        }       

        //Test();
        InteractionController.Instance.placementReady = true;

        string modelPath = GetModelPath(path);
        InteractionController.Instance.modelPath = modelPath;

        switch (model.type)
        {
            case Settings.ModelType.Image:
                Set2DModel(path, model.dimmensions);
                break;
            case Settings.ModelType.Model:
                Set3DModel(modelPath);
                break;
            default:
                break;
        }

    }


    void Set2DModel(string storePath, Vector2 dimmensions)
    {
        string imgPath = "";
        string[] imgs = Directory.GetFiles(storePath);

        if (imgs.Length == 0)
        {
            return;
        }

        imgPath = imgs[0];

        byte[] bytes = File.ReadAllBytes(imgPath);
        Texture2D texture = new Texture2D(1, 1);
        texture.LoadImage(bytes);

        //Material myNewMaterial = new Material(Shader.Find("Standard"));
        //myNewMaterial.SetTexture("_MainTex", texture);
        //myNewMaterial.SetFloat("_Mode", 1);
        //myNewMaterial.SetFloat("_Mode", 3);
        //Main.Instance.quad.GetComponent<MeshRenderer>().material = Main.Instance.myNewMaterial2;

        //Main.Instance.quad.GetComponent<MeshRenderer>().material.mainTexture = texture;
        Main.Instance.quad.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.mainTexture = texture;

        InteractionController.Instance.objectToPlace = Main.Instance.quad;
        InteractionController.Instance.placementReady = true;

#if UNITY_WSA
        ShowTutorial();

        InteractionController.Instance.objectToPlace = Instantiate(InteractionController.Instance.objectToPlace, new Vector3(), new Quaternion(), Main.Instance.imageTarget.transform);
        GameObject model = InteractionController.Instance.objectToPlace;


        model.transform.SetParent(Main.Instance.imageTarget.transform);
        model.transform.localScale = new Vector3(dimmensions.x * 5.25f / 100, dimmensions.y * 5.25f / 100, 5.25f); //5.25 jer je marker 19cm sto je skoro 1/5 od 1m, pa je model skaliran kao 5 puta manji kad se pojavi
        model.transform.localPosition = new Vector3(0, 0, 0);//dimmensions.y * 5.25f/ 200
        model.transform.GetChild(0).transform.localPosition = new Vector3(0, 0.5f, 0);

        model.AddComponent<LeanTranslate>();
        model.AddComponent<LeanScale>();
        model.AddComponent<LeanRotateCustomAxis>().Axis = Vector3.up;

        //Axis.is3dModel = false;
        //model.transform.GetChild(0).gameObject.AddComponent<Ruler2d>();
        ////Main.Instance._measurements2d.Activate(true);

        Main.Instance.ProductPlaced = true;

        InteractionController.Instance.objectToPlace.AddComponent<FloatingObject>();
        InteractionController.Instance.EnableLeanComponents(Prefs.SizeLock);
        Main.Instance.guiController.Mode = AppMode.MODEL;
        InteractionController.Instance.posTransform = new TransformParameters(model.transform.localPosition, model.transform.localEulerAngles, model.transform.localScale);

        Axis.is3dModel = false;
        //model.AddComponent<Ruler2d>();
        model.transform.GetChild(0).gameObject.AddComponent<Ruler2d>();
        Main.Instance._measurements2d.Activate(Prefs.RulerAcive);
#endif

    }

    void Set3DModel(string modelPath)
    {
        //Test(objPath);
        //Test(Application.dataPath + "/BretonBox/v_3cb_1sjcagad1/BretonBox.obj");
        /*
        try
        {
            InteractionController.Instance.objectToPlace = new OBJLoader().Load(objPath);
        }
        catch (Exception e)
        {
            Debug.Log("FileDownloading: LoadPos412 + " + e.ToString() + "\n" + e.Message);
            return;
        }

        if (InteractionController.Instance.objectToPlace == null)
        {
            Debug.Log("FileDownloading: LoadPos412 + ");
            return;
        }*/
        /*
        GameObject model = InteractionController.Instance.objectToPlace;

        float scaleX = model.transform.localScale.x;
        float sign = scaleX / Math.Abs(scaleX);

        model.transform.localScale = new Vector3(sign * scale.x, scale.y, scale.z);
        */

#if UNITY_WSA
        if (modelPath.Equals(""))
        {
            return;
        }


        ShowTutorial();

        //InteractionController.Instance.InstantiateModel(modelPath);
        InteractionController.Instance.InstantiateModelAsync(modelPath);
        /*
        GameObject model = InteractionController.Instance.objectToPlace;

        model.transform.SetParent(Main.Instance.imageTarget.transform);
        model.transform.localPosition = new Vector3(0, 0, 0);
        model.transform.localRotation = Quaternion.identity;

        model.AddComponent<LeanTranslate>();
        model.AddComponent<LeanScale>();
        model.AddComponent<LeanRotateCustomAxis>().Axis = Vector3.up;
        Main.Instance.ProductPlaced = true;

        InteractionController.Instance.posTransform = new TransformParameters(model.transform.localPosition, model.transform.localEulerAngles, model.transform.localScale);

        Axis.is3dModel = true;
        model.AddComponent<Ruler>();
        Main.Instance._measurements.Activate(Prefs.RulerAcive);*/
#endif
        /*
        byte[] bytes = File.ReadAllBytes(pngPath);
        Texture2D texture = new Texture2D(1, 1);
        texture.LoadImage(bytes);
        
        Material myNewMaterial = new Material(Shader.Find("Standard"));
        myNewMaterial.SetTexture("_MainTex", texture);
        model.transform.GetChild(0).GetComponent<MeshRenderer>().material = myNewMaterial;
        */
        //model.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.mainTexture = texture;
    }

    string GetModelPath(string path)
    {
        string[] files = Directory.GetFiles(path);
        //JOKAN TODO OVO NE RADI
        foreach (var item in Directory.GetFiles(path))
        {
            if (IsFileAModel(item))
            {
                return item;
            }
        }
        return "";
    }

    bool IsFileAModel(string path)
    {
        string ext = path.Substring(path.Length - 3);
        return (ext.Equals("3ds") || ext.Equals("3DS") || ext.Equals("obj") || ext.Equals("OBJ") || ext.Equals("fbx") || ext.Equals("FBX"));
    }

    void ShowTutorial()
    {
        if (!ProductPopup._tutorialWatched || !Util.IsTrackingGood())
        {
            Main.Instance.TutorialRoot.SetActive(true);
            PopupManager.Instance.OpenPopup(PopupType.INFO, new Dictionary<string, object>() { { "message", LocalizationController.Instance.GetTranslation(Settings.SCAN_SURFACE) } });
            Main.Instance.StartCoroutine(Main.Instance.TurnOffTutorialInSec(5f));
        }
    }

    bool IsModelStored(string path)
    {
        if (Directory.GetFiles(path).Length > 0)
        {
            return true;
        }
        return false;
        /*
        string[] folders = System.IO.Directory.GetDirectories(path);
        if (System.IO.Directory.GetDirectories(path).Length != 0)
        {
            if (System.IO.Directory.GetFiles(folders[0]).Length > 0)
            {
                return folders[0];
            }
            return "";
        }
        return "";*/
    }

    void Unzip(string zipPath, string savePath)
    {
        Debug.Log("FileDownloading: LoadPOS321 (" + zipPath + ", " + savePath + ")");
        if (File.Exists(zipPath) == false)
        {
            Debug.Log("FileDownloading: LoadPOS322 (File.Exists(zipPath) == false)");
            Debug.LogError(zipPath + "is not found!");
            //System.Diagnostics.Process.Start(Path.GetDirectoryName(zipPath));
            return;
        }

        Debug.Log("FileDownloading: LoadPOS323 (File.Exists(zipPath) != false)");
#if UNITY_WSA && !UNITY_EDITOR
        System.IO.Compression.ZipFile.ExtractToDirectory(zipPath, savePath);
#elif UNITY_EDITOR || !UNITY_WSA
        ZipUtil.Unzip(zipPath, savePath);
#endif
        Debug.Log("FileDownloading: LoadPOS324 + Path.GetDirectoryName(zipPath) = " + Path.GetDirectoryName(zipPath));
        try
        {
            //System.Diagnostics.Process.Start(Path.GetDirectoryName(zipPath));
        } catch(Exception e)
        {
            Debug.Log("FileDownloading: LoadPOS3251 exception e = " + e.ToString() + "\n" + e.Message);
        }
        Debug.Log("FileDownloading: LoadPOS325");
        File.Delete(zipPath);
        Debug.Log("FileDownloading: LoadPOS325 zip deleted");
        SetUpFiles(savePath);
    }

    void SetUpFiles(string path)
    {
        string directory = "";
        Debug.Log("LoadPOS SetUpFiles ");
        if (Directory.GetFiles(path).Length > 0)
        {
            Debug.Log("LoadPOS SetUpFiles return");
            return;
        }

        foreach (string dirFile in System.IO.Directory.GetDirectories(path))
        {
            foreach (string fileName in Directory.GetFiles(dirFile))
            {
                // fileName  is the file name
                Debug.Log("LoadPOS move files");
                Debug.Log("LoadPOS fileName = " + fileName);

                string pathName = "";
#if (!UNITY_IOS && !UNITY_ANDROID) || UNITY_EDITOR
                string name = fileName.Substring(fileName.LastIndexOf('\\') + 1);
#else
                string name = fileName.Substring(fileName.LastIndexOf('/') + 1);                
#endif

                pathName = path + "/" + name;
                Debug.Log("LoadPOS name = " + name);
                Debug.Log("LoadPOS fileName = " + fileName);
                Debug.Log("LoadPOS pathName = " + pathName);
                Debug.Log("LoadPOS fileName = " + File.Exists(fileName));
                Debug.Log("LoadPOS pathName = " + File.Exists(pathName));               
                System.IO.File.Move(fileName, pathName);
            }
            Debug.Log("LoadPOS Delete dir");
            Directory.Delete(dirFile);
        }
    }

    /*IEnumerator UnzipAndroid(string zipPath, string savePath)
    {
        Debug.Log("FileDownloading: LoadPOS321 (" + zipPath + ", " + savePath + ")");
        if (File.Exists(zipPath) == false)
        {
            Debug.Log("FileDownloading: LoadPOS322 (File.Exists(zipPath) == false)");
            Debug.LogError(zipPath + "is not found!");
            //System.Diagnostics.Process.Start(Path.GetDirectoryName(zipPath));
            yield break;
        }

        Debug.Log("FileDownloading: LoadPOS323 (File.Exists(zipPath) != false)");
#if UNITY_WSA && !UNITY_EDITOR
        System.IO.Compression.ZipFile.ExtractToDirectory(zipPath, savePath);
#else
        ZipUtil.Unzip(zipPath, savePath);
#endif
        Debug.Log("FileDownloading: LoadPOS324 + Path.GetDirectoryName(zipPath) = " + Path.GetDirectoryName(zipPath));
        yield return new WaitForSeconds(1f);
        try
        {
            //System.Diagnostics.Process.Start(Path.GetDirectoryName(zipPath));
        }
        catch (Exception e)
        {
            Debug.Log("FileDownloading: LoadPOS3251 exception e = " + e.ToString() + "\n" + e.Message);
        }
        Debug.Log("FileDownloading: LoadPOS325");
    }*/
    void SaveModelData(string name)
    {
        if (!Settings.StoredModels.ContainsKey(name))
        {
            Settings.StoredModels.Add(name, Settings.PlatformModels[name]);
        }
        else
        {
            Settings.StoredModels[name] = Settings.PlatformModels[name];
        }
        Prefs.SetJsonDataPref(JsonConvert.SerializeObject(Settings.StoredModels));
    }

//    private IEnumerator LoadBundle(string name)
//    {
//        WWW asset = WWW.LoadFromCacheOrDownload(Settings.GetBundleUrl(name), Settings.GetBundleIntVersion(name));
//        yield return asset;
//        while (!asset.isDone)
//        {
//            yield return null;
//        }
//
//        if (!string.IsNullOrEmpty(asset.error))
//        {
//            /*Debug.Log(asset.url);
//            Debug.Log("Asetbundle url Load Error".GetColoredString("red") + asset.error);
//            StartCoroutine(LoadAssetBundleFromStreamingAssets());*/
//        }
//        else
//        {
//            currentAssetBundle = asset.assetBundle;
//
//            if (currentAssetBundle != null)
//            {
//                Debug.Log("JOKAN _currentAssetBundle != null");
//                GameObject assetBundle = asset.assetBundle.LoadAsset("POS") as GameObject;
//
//                InteractionController.Instance.objectToPlace = assetBundle;
//                //InteractionController.Instance.placementReady = true;
//
//
//
//                /*GameObject posmaterialObj = Instantiate(assetBundle);
//                if (currentPOS != null)
//                {
//                    Destroy(currentPOS);
//                }
//                currentPOS = posmaterialObj;
//                currentPOS.SetActive(false);
//
//
//                Transform transform = posmaterialObj.transform;
//                transform.SetParent(imageTarget.transform);
//                transform.localPosition = new Vector3(0, 0, 0);
//                transform.localEulerAngles = new Vector3(0, 0, 0);
//                //transform.localScale = new Vector3(2, 2, 2);
//                posmaterialObj.SetActive(true);*/
//
//                //UIMain.ShowMessage("Select a POS");
//                //UIMain.ShowProgress(false);
//            }
//            else
//            {
//                Debug.Log("currentAssetBundle == null");
//                //UIMain.ShowProgress(false);
//                // DebugTest.text += "\n asset null " + asset.error;
//            }
//        }
//    }

    public void GetAListOfFiles()
    {
        // Get the object used to communicate with the server.
        /*FtpWebRequest request = (FtpWebRequest)WebRequest.Create("http://arcontent.spinview.io/MerckAR/");
        request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

        // This example assumes the FTP site uses anonymous logon.
        //request.Credentials = new NetworkCredential("anonymous", "janeDoe@contoso.com");

        FtpWebResponse response = (FtpWebResponse)request.GetResponse();

        Stream responseStream = response.GetResponseStream();
        StreamReader reader = new StreamReader(responseStream);
        //Console.WriteLine(reader.ReadToEnd());
        Debug.Log(reader.ReadToEnd());

        //Console.WriteLine($"Directory List Complete, status {response.StatusDescription}");
        Debug.Log("Directory List Complete, status = " + response.StatusDescription);
        reader.Close();
        response.Close();*/
    }

}

