﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class WebRequest
{
    static int TOKENCNTR = 0;
    public static int GetNewToken()
    {
        return TOKENCNTR++;
    }
    public static IEnumerator Get(int token, string link, bool texture = false, bool startLoading = false)
    {
        for (int i = 0; i < 5; i++)
        {
            UnityWebRequest www = texture == false ? UnityWebRequest.Get(link) : UnityWebRequestTexture.GetTexture(link);
            www.chunkedTransfer = false;
            if(startLoading && Main.Instance != null)
                Main.Instance.loading.StartLoading(www);
            yield return www.SendWebRequest();
            if (string.IsNullOrEmpty(www.error))
            {
                if(Main.Instance != null) Main.Instance.loading.EndLoading();
                _data.Add(token, www);
                break;
            }
            yield return new WaitForSeconds(0.25f);
        }
    }
    static Dictionary<int, UnityWebRequest> _data = new Dictionary<int, UnityWebRequest>();
    public static UnityWebRequest WWW(int token)
    {
        if (_data.ContainsKey(token)) return _data[token];
        return null;
    }
    public static string StringData(int token)
    {
        if (_data.ContainsKey(token)) return _data[token].downloadHandler.text;
        return string.Empty;
    }
    public static byte[] BytesData(int token)
    {
        if (_data.ContainsKey(token)) return _data[token].downloadHandler.data;
        return null;
    }
    public static Texture2D TextureData(int token)
    {
        if (_data.ContainsKey(token)) return DownloadHandlerTexture.GetContent(_data[token]);
        return null;
    }
    public static bool Error(int token)
    {
        if (_data.ContainsKey(token)) return _data[token].isNetworkError;
        return true;
    }
    public static void RemoveData(int token)
    {
        _data.Remove(token);
    }
}
