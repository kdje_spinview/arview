﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FloatingMode
{
    GROUND,
    FLOATING
}

public class FloatingObject : MonoBehaviour
{
    #region objects_state
    enum FloatingState
    {
        ONTHEGROUND,
        FLOATING,
        MOVINGUP,
        MOVINGDOWN,
        GROUNDJUMP
    }
    FloatingState _state = FloatingState.ONTHEGROUND;
    float _animationStartTime;
    float _startingPositionY;
    float _groundY = 0f;
    FloatingState _State
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
            _animationStartTime = Time.time;
            _startingPositionY = transform.localPosition.y;
        }
    }
    FloatingMode _mode = FloatingMode.GROUND;
    public FloatingMode Mode
    {
        set
        {
            if (_mode == FloatingMode.GROUND && value == FloatingMode.FLOATING)
                _groundY = transform.localPosition.y;
            _mode = value;
        }
    }
    #endregion

    #region consts
    const float FLOATHEIGHT = 0.055f; //windows: 0.1 x marker_width; iOS & Android: 0.1m
    const float FLOATOFFSET = 0.02f; //-||-
    const float FLOATINGCYCLEDURATION = 3f; //duration of one duration cycle (going up and down)
    const float MOVINGUPDURATION = 0.4f; //duration in seconds of MOVINGUP state
    const float MOVINGDOWNDURATION = 0.5f; //duration in seconds of MOVINGDOWN state
    const float GROUNDJUMPDURATION = 0.15f; //duration of ground jump after moving down and before ontheground states
    const float GROUNDOFFSET = 0.005f; //offset of ground jump
    #endregion

    private void Start()
    {
        _animationStartTime = Time.time;
        _startingPositionY = 0f;
        Mode = FloatingMode.FLOATING;
        _State = FloatingState.ONTHEGROUND;
    }
    private void Update()
    {
        switch (_mode)
        {
            case FloatingMode.GROUND:
                switch (_State)
                {
                    case FloatingState.ONTHEGROUND:
                        return;
                    case FloatingState.FLOATING:
                    case FloatingState.MOVINGUP:
                        _State = FloatingState.MOVINGDOWN;
                        return;
                    case FloatingState.MOVINGDOWN:
                        {
                            float t = (Time.time - _animationStartTime) / MOVINGDOWNDURATION;
                            transform.localPosition = new Vector3(transform.localPosition.x, Mathf.SmoothStep(_startingPositionY, _groundY/*0f*/, t), transform.localPosition.z);
                            if (t >= 1f)
                                _State = FloatingState.GROUNDJUMP;
                        }
                        break;
                    case FloatingState.GROUNDJUMP:
                        {
                            float t = (Time.time - _animationStartTime) / GROUNDJUMPDURATION;
                            transform.localPosition = new Vector3(transform.localPosition.x, _groundY + GROUNDOFFSET * Mathf.Sin(t * Mathf.PI), transform.localPosition.z);
                            if (t > 1f)
                                _State = FloatingState.ONTHEGROUND;
                        }
                        break;
                }
                break;
            case FloatingMode.FLOATING:
                switch (_State)
                {
                    case FloatingState.ONTHEGROUND:
                    case FloatingState.GROUNDJUMP:
                    case FloatingState.MOVINGDOWN:
                        _State = FloatingState.MOVINGUP;
                        return;
                    case FloatingState.FLOATING:
                        {
                            float t = (Time.time - _animationStartTime) / FLOATINGCYCLEDURATION;
                            transform.localPosition = new Vector3(transform.localPosition.x, _groundY + FLOATHEIGHT + FLOATOFFSET * Mathf.Sin(t * 2 * Mathf.PI), transform.localPosition.z);
                        }
                        break;
                    case FloatingState.MOVINGUP:
                        {
                            float t = (Time.time - _animationStartTime) / MOVINGUPDURATION;
                            transform.localPosition = new Vector3(transform.localPosition.x, Mathf.SmoothStep(_startingPositionY, _groundY + FLOATHEIGHT, t), transform.localPosition.z);
                            if (t >= 1f)
                                _State = FloatingState.FLOATING;
                        }
                        break;
                }
                break;
        }
    }
}
