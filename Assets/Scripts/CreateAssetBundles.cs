﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class CreateAssetBundles : MonoBehaviour
{

    [MenuItem("Assets/Build AssetBundles")]

    static void BuildAllAssetBundles()
    {
        //string folderName = System.DateTime.Now.Millisecond.ToString();
        BuildTarget target = BuildTarget.iOS; //BuildTarget.WSAPlayer za windows


        string filePath = "AssetBundles/" + target.ToString() + "/" + System.DateTime.Now.Ticks.ToString();

        Directory.CreateDirectory(filePath);

        //System.IO.FileInfo file = new System.IO.FileInfo(filePath);
        //file.Directory.Create();

        BuildPipeline.BuildAssetBundles(filePath, BuildAssetBundleOptions.DisableWriteTypeTree, target);
        //BuildPipeline.BuildAssetBundles(filePath, BuildAssetBundleOptions.DisableWriteTypeTree, target);
    }
}
#endif