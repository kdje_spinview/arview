﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
//using System.Runtime.Serialization.Formatters.Binary;
//using System.IO;


public class ForgotPassword : MonoBehaviour
{
    public InputField EmailInputField;
    public Button SendResetPasswordButton;
    public Button GoBackButton;
    public Button CheckInboxGoBackButton;


    private void Awake()
    {
        SendResetPasswordButton.onClick.AddListener(SendRequestPassword);
        GoBackButton.onClick.AddListener(() => { LogIn.instance.ActivateLogInRoot(LogIn.LogInRoot.LOGIN); });
        CheckInboxGoBackButton.onClick.AddListener(() => { LogIn.instance.ActivateLogInRoot(LogIn.LogInRoot.LOGIN); });
        EmailInputField.onValueChanged.AddListener(EmailChanged);
        EmailInputField.text = string.Empty;
    }

    void SendRequestPassword()
    {
        StartCoroutine(SendResetPassword());
    }
    void EmailChanged(string email)
    {
        SendResetPasswordButton.interactable = !(string.IsNullOrEmpty(email) || !Util.IsEmail(email));
    }
    IEnumerator SendResetPassword()
    {
        SendResetPasswordButton.interactable = false;
        LogIn.instance.loadingBlur.Activate("sending");

        UnityWebRequest www = null;

        try
        {
            //List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
            //formData.Add(new MultipartFormDataSection("email", EmailInputField.text));

            byte[] myData = System.Text.Encoding.UTF8.GetBytes("{\"email\":\"" + EmailInputField.text + "\"}");
            //byte[] myData = ObjectToByteArray(formData);

            www = UnityWebRequest.Put("https://api.spinviewglobal.com/api/v1/users/reset-password/request", myData);
            www.SetRequestHeader("Content-Type", "application/json");
            //www.method = UnityWebRequest.kHttpVerbPUT;

        }
        catch (Exception e)
        {
            Debug.Log("MailingSystem>SendMessage error = " + e.ToString());
            Close(www);
            yield break;
        }
        
        yield return www.SendWebRequest();

        Close(www);
    }
    /*private byte[] ObjectToByteArray(object obj)
    {
        if (obj == null)
            return null;
        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        bf.Serialize(ms, obj);
        return ms.ToArray();
    }*/
    void Close(UnityWebRequest www)
    {
        LogIn.instance.loadingBlur.Deactivate();
        if (www == null ||  www.isHttpError)
        {
            LogIn.instance.Notify(Settings.SOMETHING_WENT_WRONG);
            SendResetPasswordButton.interactable = true;
        } else if (www.isNetworkError)
        {
            LogIn.instance.Notify(Settings.NO_INTERNET);
            SendResetPasswordButton.interactable = true;
        }
        else
        {
            //LogIn.instance.Notify(Settings.EMAIL_SENT);
            LogIn.instance.ActivateLogInRoot(LogIn.LogInRoot.CHECKYOUREMAIL);
        }
    }
}
