﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

/*old info
public enum CloseConfition
{
    TRACKINGBAD,
    AFTERSECONDS
}*/
public class Info : MonoBehaviour
{
    
    //new info:
    enum InfoState
    {
        NO_MODEL,//show tutorial or not
        MODEL_SELECTED_BUT_NOT_PLACED,//only for mobile - model selected from product popup, but not placed yet
        MODEL_PLACED//
    }
    public static bool ModelSelected = false;
    public static bool ModelPlaced = false;
    InfoState CurrentState
    {
        get
        {
            if (!ModelSelected) return InfoState.NO_MODEL;

#if UNITY_WSA
            return InfoState.MODEL_PLACED;
#endif
            if (ModelPlaced) return InfoState.MODEL_PLACED;

            return InfoState.MODEL_SELECTED_BUT_NOT_PLACED;
            
        }
    }
    private void Start()
    {
        StartCoroutine(CheckInternetConnection());
        StartCoroutine(CheckInfo());
    }
    IEnumerator CheckInfo()
    {
        while (true)
        {
            switch (CurrentState)
            {
                case InfoState.NO_MODEL:

                    break;
                case InfoState.MODEL_SELECTED_BUT_NOT_PLACED:

                    break;
                case InfoState.MODEL_PLACED:

                    break;
            }


            yield return new WaitForSeconds(1f);
        }
    }


    bool InternetConnection;
    IEnumerator CheckInternetConnection()
    {
        while (true)
        {
            UnityWebRequest www = new UnityWebRequest("http://google.com");
            yield return www;

            InternetConnection = (www.error == null);

            yield return new WaitForSeconds(1f);
        }
    }
    /*old info
    public static bool Shown = false;

    public Animator _animator;
    public Text MessageText;
    CloseConfition _closeCondition;
    public CloseConfition CloseCond
    {
        private get
        {
            return _closeCondition;
        }
        set
        {
            _closeCondition = value;

            switch (_closeCondition)
            {
                case CloseConfition.TRACKINGBAD:
                    StartCoroutine(CheckTracking());
                    break;
                case CloseConfition.AFTERSECONDS:
                    break;
            }

        }
    }
    private void Awake()
    {
        Shown = true;
    }
    IEnumerator CheckTracking()
    {
        while (!Util.IsTrackingGood()) yield return null;

        yield return Close();
    }
    IEnumerator Close()
    {
        _animator.Play("InfoTransparencyReverse");


        yield return new WaitForSeconds(1f);
        Shown = false;
        Destroy(gameObject);
    }*/
}
