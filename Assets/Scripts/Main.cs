﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Main : MonoBehaviour {

#if UNITY_WSA
    //TrackableBehaviour.Status _vuforiaStatus;
#endif
    public GameObject mainUI;
    //portrait orientation//public GameObject portraitUI;
    //portrait orientation//public GameObject landscapeUI;
    public GameObject imageTarget;

    public GameObject screenshotAnimation;
    public GameObject objRoot;
    
    public GameObject arVuforia;

    public GuiController guiController;
    public MapLocation mapLocation;
    public GameObject buttons;
    public Transform NotificationsRoot;
    public GameObject NotificationPrefab;
    public LoadingBar loading;
    public RawImage CompanyRawImage;
    public RectTransform canvasRectTransform;
    public CompanyLoading _companyLoading;
    public LoadingBlur loadingBlur;
    public Measurements _measurements;
    public Measurements2d _measurements2d;

    public Transform PopupRoot;
    public GameObject quad;

    public GameObject arRoot;
    public GameObject welcome;

    public Button LetsGoButton;

    bool _productPlaced = false;
    public bool ProductPlaced
    {
        get
        {
            return _productPlaced;
        }
        set
        {
            _productPlaced = value;
            Main.Instance.guiController.ResetButton.gameObject.SetActive(value);
        }
    }

    public Button ModelNotAddedButton;

    public Button TutorialRootButton;
    public GameObject TutorialRoot;

    //portrait orientation//Orientation _currentOrientation;

    private void Awake()
    {
        ModelNotAddedButton.onClick.AddListener(() => { Notify(Settings.SELECT_PRODUCT_FIRST); });
        TutorialRootButton.onClick.AddListener(() => { TutorialRoot.SetActive(false); ProductPopup._tutorialWatched = true; });
        ProductPopup._currentCompany = "";

        Settings.arSSFolderID = "";

        const float defaultValue = 5f;
        UnityEngine.EventSystems.EventSystem.current.pixelDragThreshold = (int)Mathf.Max(defaultValue, (int)(defaultValue * Screen.dpi / 200f));
#if UNITY_WSA
        arVuforia.SetActive(true);
        welcome.SetActive(false);
#else
#if UNITY_ANDROID
        //StartCoroutine(FadeWelcome(0f, 1f, 0.5f, true));
        welcome.GetComponent<Animator>().Play("WelcomeFadeIn");
        LetsGoButton.onClick.AddListener(() => { StartAR(); });
#endif
        arVuforia.SetActive(false);
#endif
        //PlayerPrefs.DeleteAll();
#if UNITY_EDITOR
        LocalizationController.Instance.CurrentLanguage = Prefs.Language;
#endif
    }
    /*IEnumerator FadeWelcome(float startAlpha, float endAlpha, float time, bool activate)
    {
        if(activate)
            welcome.SetActive(true);
        CanvasGroup cg = welcome.GetComponent<CanvasGroup>();
        float startTime = Time.time;
        float endTime = Time.time + time;
        while(Time.time < endTime)
        {
            cg.alpha = Mathf.Lerp(startAlpha, endAlpha, Time.time - startTime);
            yield return null;
        }
        cg.alpha = endAlpha;
        if (!activate)
            welcome.SetActive(false);
    }*/
    public IEnumerator TurnOffTutorialInSec(float sec)
    {
        //yield return new WaitForSeconds(sec);

        float startTime = Time.time;

        while(Time.time - startTime < sec)
        {
            if (Util.IsTrackingGood()) break;
            yield return null;
        }

        TutorialRoot.SetActive(false);
        ProductPopup._tutorialWatched = true;
        
        while (true)
        {
            Popup _popupInfo = PopupManager.Instance.GetPopup(PopupType.INFO);
            if (_popupInfo == null) yield break; // already closed

            Popup _lastPopup = PopupManager.Instance.GetLastPopup();
            if (_lastPopup == null) continue;
            if (_lastPopup.Type == PopupType.INFO)
            {
                PopupManager.Instance.ClosePopup();
                yield break;
            }
            yield return new WaitForSeconds(sec);
        }
    }
    // Use this for initialization
    void Start()
    {
        //bool connection = false;
        StartCoroutine(Main.Instance.CheckInternetConnection((isConnected) => {
            // handle connection status here
            //connection = isConnected;
            if(!isConnected)
                Notify(Settings.NO_INTERNET);
        }));

        //if (!connection)
        //{
        //    Notify(Settings.NO_INTERNET);
        //}
        //else
        {
            InitMap();
            
            //jsonReader.LoadData();
            //CreatePOSListDemo();

            _companyLoading.StartLoading();
        }

        //portrait orientation//_currentOrientation = Orientation.NONE;
        //portrait orientation//StartCoroutine(UpdateOrientation());

        ScaleToggleOn = ScaleToggleOn;
#if !UNITY_WSA && !UNITY_ANDROID
        StartAR();
#endif
    }

#if !UNITY_WSA
    public void StartAR()
    {
        GameObject ar = Instantiate(arRoot);
        InteractionController.Instance.SetupAR(ar.transform.GetChild(0).gameObject);

#if UNITY_ANDROID
        //StartCoroutine(FadeWelcome(1f, 0f, 0.5f, false));
        welcome.GetComponent<Animator>().Play("WelcomeFadeOut");        
#endif
    }
#endif

    /*//portrait orientation//IEnumerator UpdateOrientation()
    {
        while (true)
        {

            yield return new WaitForSeconds(0.25f);
            Orientation newOrientation = Util.GetOrientation();
            if (_currentOrientation != newOrientation)
            {
                OnScreenRotate(newOrientation);
                _currentOrientation = newOrientation;
            }
        }
    }
    void OnScreenRotate(Orientation newOrientation)
    {
        switch (newOrientation)
        {
            case Orientation.LANDSCAPE:
                landscapeUI.SetActive(false);
                portraitUI.SetActive(true);
                break;
            case Orientation.PORTRAIT:
                landscapeUI.SetActive(true);
                portraitUI.SetActive(false);
                break;
        }
    }//portrait orientation//*/
    void InitMap()
    {
    //Bug: first map is not saved properly. 
    //Fix: force map image creation at the scene start
        mapLocation.CreateMapImage(/*Map"*/false);
    }

    //for deleting cached asset bundles
    public static void CleanCache()
    {
        if (Caching.ClearCache())
        {
            Debug.Log("Successfully cleaned the cache.");
        }
        else
        {
            Debug.Log("Cache is being used.");
        }
    }

    /*public void CreatePOSListDemo()
    {
        for (int i = 0; i < models.Count; i++)
        {
            //create buttons portrait
            GameObject button = Instantiate(posButtonPrefabPortrait);
            button.GetComponent<POSButton>().SetButton((i + 1).ToString());
            Transform transform = button.transform;
            transform.SetParent(posButtonsRootPortrait.transform);
            transform.localPosition = new Vector3(0, 0, 0);
            transform.localScale = new Vector3(1, 1, 1);
            transform.localRotation = new Quaternion(0, 0, 0, 0);
            //create buttons landscape
            GameObject button2 = Instantiate(posButtonPrefabLandscape);
            button2.GetComponent<POSButton>().SetButton((i + 1).ToString());
            Transform transform2 = button2.transform;
            transform2.SetParent(posButtonsRootLandscape.transform);
            transform2.localPosition = new Vector3(0, 0, 0);
            transform2.localScale = new Vector3(1, 1, 1);
            transform2.localRotation = new Quaternion(0, 0, 0, 0);
        }
    }

    public void CreatePOSList(Dictionary<string, Sprite> sprites)
    {
        foreach(var item in Settings.PlatformModels)
        {
            //create buttons portrait
            GameObject button = Instantiate(posButtonPrefabPortrait);
            button.GetComponent<POSButton>().SetButton(item.Key, sprites[item.Key]);
            Transform transform = button.transform;
            transform.SetParent(posButtonsRootPortrait.transform);
            transform.localPosition = new Vector3(0, 0, 0);
            transform.localScale = new Vector3(1, 1, 1);
            transform.localRotation = new Quaternion(0, 0, 0, 0);
            //create buttons landscape
            GameObject button2 = Instantiate(posButtonPrefabLandscape);
            button2.GetComponent<POSButton>().SetButton(item.Key, sprites[item.Key]);
            Transform transform2 = button2.transform;
            transform2.SetParent(posButtonsRootLandscape.transform);
            transform2.localPosition = new Vector3(0, 0, 0);
            transform2.localScale = new Vector3(1, 1, 1);
            transform2.localRotation = new Quaternion(0, 0, 0, 0);
        }
    }*/
    //because windows tablets are making an issue of sometimes switching portrait and landscape
    /*void OnScreenRotate3(DeviceOrientation previousOrientation, DeviceOrientation newOrientation)
    {
        if ((previousOrientation == DeviceOrientation.Portrait && newOrientation == DeviceOrientation.PortraitUpsideDown) || (previousOrientation == DeviceOrientation.LandscapeLeft && newOrientation == DeviceOrientation.LandscapeRight) ||
            (previousOrientation == DeviceOrientation.PortraitUpsideDown && newOrientation == DeviceOrientation.Portrait) || (previousOrientation == DeviceOrientation.LandscapeRight && newOrientation == DeviceOrientation.LandscapeLeft))
        {
            return;
        }
        switch (newOrientation)
        {
            case DeviceOrientation.LandscapeLeft:
            case DeviceOrientation.LandscapeRight:
            case DeviceOrientation.Portrait:
            case DeviceOrientation.PortraitUpsideDown:
                if (Util.ScreenWidth > Util.ScreenHeight)
                {
                    landscapeUI.SetActive(false);
                    portraitUI.SetActive(true);
                    posMenuPortrait.SetActive(posMenuPortrait.activeSelf || posMenuLandscape.activeSelf ? true : false);
                    posMenuLandscape.SetActive(false);
                    break;
                }
                else
                {
                    landscapeUI.SetActive(true);
                    portraitUI.SetActive(false);
                    posMenuLandscape.SetActive(posMenuPortrait.activeSelf || posMenuLandscape.activeSelf ? true : false);
                    posMenuPortrait.SetActive(false);
                    break;
                }
            case DeviceOrientation.FaceUp:
            case DeviceOrientation.FaceDown:
            case DeviceOrientation.Unknown:
                break;
            default:
                break;
        }
    }*/

    /*void OnScreenRotate2(DeviceOrientation orientation)
    {
        switch (orientation)
        {
#if UNITY_ANDROID || UNITY_EDITOR || UNITY_IOS
            case DeviceOrientation.LandscapeLeft:
            case DeviceOrientation.LandscapeRight:
#elif UNITY_WSA && !UNITY_EDITOR
            case DeviceOrientation.Portrait:
            case DeviceOrientation.PortraitUpsideDown:
#endif
                landscapeUI.SetActive(false);
                portraitUI.SetActive(true);
                posMenuPortrait.SetActive(posMenuLandscape.activeSelf);
                posMenuLandscape.SetActive(false);                
                break;
#if UNITY_ANDROID || UNITY_EDITOR || UNITY_IOS
            case DeviceOrientation.Portrait:
            case DeviceOrientation.PortraitUpsideDown:
#elif UNITY_WSA && !UNITY_EDITOR
            case DeviceOrientation.LandscapeLeft:
            case DeviceOrientation.LandscapeRight:
#endif
                landscapeUI.SetActive(true);
                portraitUI.SetActive(false);
                posMenuLandscape.SetActive(posMenuPortrait.activeSelf);
                posMenuPortrait.SetActive(false);
                break;
            case DeviceOrientation.FaceUp:
            case DeviceOrientation.FaceDown:
            case DeviceOrientation.Unknown:
                break;
            default:
                break;
        }
    }*/

    public IEnumerator CheckInternetConnection(Action<bool> action)
    {
        UnityWebRequest www = new UnityWebRequest("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    }

    public void Notify(string messageKey)
    {
        GameObject notification = Instantiate(NotificationPrefab, NotificationsRoot);
        LocalizationElement lE = notification.GetComponentInChildren<LocalizationElement>();
        lE.Key = messageKey;
        lE.SetText();
        Destroy(notification, 4f);
    }

    public IEnumerator RefreshAfterOneFrame(GameObject go)
    {
        go.SetActive(false);

        //yield return new WaitForSeconds(0.1f);
        yield return new WaitForEndOfFrame();

        go.SetActive(true);
    }
#region BUTTONS
    public void OpenSettings()
    {
        buttons.SetActive(false);
        PopupManager.Instance.OpenPopup(PopupType.SETTINGS);
    }
    public void PosCatalogueButton()
    {
        Debug.Log("PosCatalogueButton");
        PopupManager.Instance.OpenPopup(PopupType.PRODUCT);
    }
    public void UndoButton()
    {
        Debug.Log("Touch: TriggerUndo");
        Transform transform = InteractionController.Instance.objectToPlace.transform;
        TransformParameters parameters = InteractionController.Instance.posTransform;
        transform.localPosition = InteractionController.Instance.posTransform.position;
        transform.localScale = InteractionController.Instance.posTransform.scale;
        transform.localEulerAngles = InteractionController.Instance.posTransform.rotation;
    }
    public void DimensionsButton()
    {
        if (InteractionController.Instance.objectToPlace != null)
        {
            GameObject dimensions = InteractionController.Instance.objectToPlace.GetComponent<POSObject>().dimensions;
            dimensions.SetActive(!dimensions.activeSelf);
        }

    }
    /*test
    public void ScreenShotButtonOld()
    {
        ScreenCapture.CaptureScreenshot("test2.png");
    }*/

    public void ScreenShotButton()
    {
        /*if (!ProductPlaced)
        {
            Notify("please_first_select_product");
#if UNITY_EDITOR || UNITY_ANDROID
            //testing:
            PopupManager.Instance.OpenPopup(PopupType.SCREENSHOT);    
#endif
            return;
        }*/
        PopupManager.Instance.ClosePopupIfLast(PopupType.INFO);
        PopupManager.Instance.OpenPopup(PopupType.SCREENSHOT);
    }
    public Sprite RulerCheckedSprite;
    public Sprite RulerUncheckedSprite;
    
    public void ActivateRulerClicked()
    {
        ActivateRulerClicked(!Prefs.RulerAcive);
    }
    public void ActivateRulerClicked(bool flag)
    {
        Prefs.RulerAcive = flag;

        guiController.RulerButtonImage.sprite = flag ? RulerCheckedSprite : RulerUncheckedSprite;

        if (Axis.is3dModel)
        {
            _measurements.Activate(flag);
            _measurements2d.Activate(false);
        }
        else
        {
            _measurements.Activate(false);
            _measurements2d.Activate(flag);
        }
    }
    public void LockClicked()
    {
        ScaleToggleOn = !ScaleToggleOn;
    }
    public Sprite ScaleLockCheckedSprite;
    public Sprite ScaleLockUncheckedSprite;
    bool ScaleToggleOn
    {
        get
        {
            return Prefs.SizeLock;
        }
        set
        {
            Prefs.SizeLock = value;
            guiController.ScaleButtonImage.sprite = value ? ScaleLockCheckedSprite : ScaleLockUncheckedSprite;
            InteractionController.Instance.EnableLeanComponents(value);

        }
    }
    public void ModelOkClicked()
    {
        guiController.Mode = AppMode.IDLE;

        InteractionController.Instance.objectToPlace.GetComponent<FloatingObject>().Mode = FloatingMode.GROUND;
    }
#endregion

#region SINGLETON
    private static Main instance = null;

    public static Main Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<Main>();
            }

            return instance;
        }
    }
#endregion

#if UNITY_EDITOR
    [MenuItem("AR/Clear Player Prefs")]
    static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("PlayerPrefs cleared");
    }
#endif

    public void ReloadProductPopup()
    {
        PopupManager.Instance.ClosePopup();
        PopupManager.Instance.OpenPopup(PopupType.PRODUCT);
    }

#region info
    /*old info public GameObject InfoPrefab;

    public void ShowInfo(string key)
    {
        GameObject infoGO = Instantiate(InfoPrefab, NotificationsRoot);
        Info info = infoGO.GetComponent<Info>();
        info.MessageText.text = LocalizationController.Instance.GetTranslation(key);

        info.CloseCond = CloseConfition.TRACKINGBAD;
    }*/
#endregion
}
