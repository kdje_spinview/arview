﻿using System.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
//using System.Net.Mail;
using System.Net.Security;
//using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Text.RegularExpressions;

public enum EmailType
{
    TO,
    CC
}
public class MailSystem : Popup {

    ScreenshotManager _screenshotPopup;
    private void Awake()
    {
        SendButton.onClick.AddListener(Send);
        CollapsedButton.onClick.AddListener(CollapseButton);
        SubjectInputField.onValueChanged.AddListener(OnSubjectInputFieldChange);
        MessageInputField.onValueChanged.AddListener(OnMessageInputFieldChange);
        ToInputField.onEndEdit.AddListener(ToChanged);
        UncollapsedToButton.onClick.AddListener(() => { UncollapsedToInputField.Select(); });
        UncollapsedCcButton.onClick.AddListener(() => { UncollapsedCcInputField.Select(); });
        UncollapsedToInputField.onEndEdit.AddListener(ToAdded);
        UncollapsedCcInputField.onEndEdit.AddListener(CcAdded);


        _screenshotPopup = (ScreenshotManager)PopupManager.Instance.GetPopup(PopupType.SCREENSHOT);
        if (_screenshotPopup == null)
        {
            PopupManager.Instance.ClosePopup();
        } else
        {//mailPopup.RemoveEmail(Text.text, _type);
            //ImageNameText.text = _screenshotPopup.emailScreenshotFilename;
            //MapImageNameText.text = _screenshotPopup.emailMapScreenshotFilename;
        }
        ScreenshotTicked = true;
        MapTicked = true;
        ScreenshotImageButton.onClick.AddListener(() => ScreenshotTicked = !ScreenshotTicked);
        MapImageButton.onClick.AddListener(() => MapTicked = !MapTicked);
    }

    #region top
    public Button SendButton;
    public Text SendButtonText;
    
    public void Send()
    {
        Debug.Log("Mailing: Send");
        string errorMessage = CheckMail();
        if (string.IsNullOrEmpty(errorMessage))
            StartCoroutine(SendMessage());
        else
            Main.Instance.Notify(errorMessage);
    }
    void SetSendButtonColor(bool enable)
    {
        Color myColor = new Color();
        ColorUtility.TryParseHtmlString(enable ? "#00a49a" : "#8b8b8b", out myColor);
        SendButtonText.color = myColor;
    }
    string CheckMail()
    {
        SetSendButtonColor(false);
        if (_emails.Count == 0) return Settings.EMAIL_NOT_VALID;
        if (string.IsNullOrEmpty(SubjectInputField.text)) return Settings.SUBJECT_EMPTY;
        SetSendButtonColor(true);
        return null;
    }

    IEnumerator SendMessage()
    {
        bool connection = false;
        yield return StartCoroutine(Main.Instance.CheckInternetConnection((isConnected) => {
            // handle connection status here
            connection = isConnected;
        }));

        if (!connection)
        {
            Main.Instance.Notify(Settings.NO_INTERNET);
            yield break;
        }

        SendButton.interactable = false;
        SendButton.GetComponentInChildren<Text>().color = Color.gray;
        Main.Instance.loadingBlur.Activate("sending");

        yield return new WaitForSeconds(1f);

        UnityWebRequest www = null;
        try
        {
            List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
            string emailList = string.Join(",", _emails);
            string ccList = string.Join(",", _ccs);
            string subject = SubjectInputField.text;
            string bodytext = BodyInputField.text;

            formData.Add(new MultipartFormDataSection("to", emailList));
            if (_ccs.Count != 0) formData.Add(new MultipartFormDataSection("cc", ccList));
            if(!string.IsNullOrEmpty(bodytext))
                formData.Add(new MultipartFormDataSection("message", bodytext));
            formData.Add(new MultipartFormDataSection("subject", subject));
            
            //if (_screenshotPopup.imageBytes != null && ScreenshotTicked) formData.Add(new MultipartFormFileSection("attachment", _screenshotPopup.imageBytes, _screenshotPopup.emailScreenshotFilename, "image"));
            //if (Main.Instance.mapLocation.mapBytes != null && MapTicked) formData.Add(new MultipartFormFileSection("attachment1", Main.Instance.mapLocation.mapBytes, _screenshotPopup.emailMapScreenshotFilename, "image"));
            
            www = UnityWebRequest.Post("api.spinviewglobal.com/api/v1/notifications/send/ar-app-notification", formData);

        }
        catch (Exception e)
        {
            Debug.Log("MailingSystem>SendMessage error = " + e.ToString());
            Close(www);
            yield break;
        }

        //Main.Instance.loading.StartLoading(www);
        yield return www.SendWebRequest();

        Close(www);
    }
    void Close(UnityWebRequest www)
    {
        Main.Instance.loadingBlur.Deactivate(); 
        if (www == null || www.isNetworkError || www.isHttpError)
        {
            Main.Instance.Notify(Settings.SOMETHING_WENT_WRONG);
            SendButton.interactable = true;
        }
        else
        {
            Main.Instance.Notify(Settings.EMAIL_SENT);
            PopupManager.Instance.ClosePopup();
        }
    }
    /*IEnumerator SendMessage2()
    {

        yield return new WaitForSeconds(1f);
        Debug.Log("Mailing: SendMessage");
        MailMessage mail = new MailMessage();

        mail.From = new MailAddress("jovan@spinview.io");//TODO: change it to spinview@spinview.io
        foreach (string email in _emails) mail.To.Add(email);
        foreach (string cc in _ccs) mail.CC.Add(cc);
        mail.Subject = SubjectInputField.text;
        string bodytext = BodyInputField.text;
        bodytext += "\n\nEmail sent from +AR View app.\n";
        mail.Body = bodytext;
        string pathh = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        Attachment attachment = null;
        try
        {
            if (Main.Instance == null) Debug.Log("Mailing: Main.Instance == null");
            if (Main.Instance.screenshotManager == null) Debug.Log("Mailing: Main.Instance.screenshotManager == null");

            Debug.Log("Mailing: attachment path try " + Application.persistentDataPath + Main.Instance.screenshotManager.emailScreenshotFilename);
            attachment = new Attachment(Application.persistentDataPath + Main.Instance.screenshotManager.emailScreenshotFilename);
            Debug.Log("Mailing: attachment path exists 2 ");
        }
        catch (Exception e)
        {
            Debug.Log("Mailing: SendMessage1 + " + e.ToString());
            Debug.Log("Mailing: SendMessage2 + " + e.Message);
        }
        Debug.Log("Mailing: SendMessage after creating attachment");
        mail.Attachments.Add(attachment);
        Debug.Log("Mailing: SendMessage after adding attachment to list");

        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        Debug.Log("Mailing: smtp server 1");
        smtpServer.Port = 587;
        Debug.Log("Mailing: smtp server 2");
        smtpServer.Credentials = new System.Net.NetworkCredential("jovan@spinview.io", "Armeddragonlv13");//TODO: change it to spinview@spinview.io
        Debug.Log("Mailing: smtp server 3");
        smtpServer.EnableSsl = true;
        Debug.Log("Mailing: smtp server 4");

        ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
                return true;
            };

        Debug.Log("Mailing: SendMessage before sending");
        try
        {
            smtpServer.Send(mail);

            Debug.Log("Mailing: SendMessage after send");
            Main.Instance.Notify(Settings.EMAIL_SENT);
            Back();
        }
        catch (Exception e)
        {
            Debug.Log("Mailing: SendMessage11 + " + e.ToString());
            Debug.Log("Mailing: SendMessage21 + " + e.Message);
            Debug.Log(e.GetBaseException());
            if (e.Message.StartsWith("535"))
            {
                //username or password wrong
                Main.Instance.Notify("invalid_username_or_password");
            }
            else
            {
                //something went wrong
                Main.Instance.Notify("something_went_wrong");
            }
        }
    }*/
    #endregion

    #region to
    float _toSize = 144f;
    float ToSize
    {
        get { return _toSize; }
        set
        {
            if (_toSize != value)
            {
                //subjectmessageRoot .position += value - size
                SubjectMessageRoot.anchoredPosition += new Vector2(0f, _toSize - value);
                _toSize = value;
            }
        }
    }
    public RectTransform SubjectMessageRoot;
    public RectTransform ToRoot;
    public GameObject CollapsedTo;
    public GameObject UncollapsedTo;
    public Button CollapsedButton;
    public InputField ToInputField;
    public GameObject MailPrefab;
    public Transform UncollapsedToRoot;
    public Transform UncollapsedCcRoot;
    public RectTransform UncollapsedToRT;
    public RectTransform UncollapsedCcRT;
    public Button UncollapsedToButton;
    public Button UncollapsedCcButton;
    public InputField UncollapsedToInputField;
    public InputField UncollapsedCcInputField;


    const float collapsedSize = 144f;
    float uncollapsedSize = 432f;//not const
    bool _collapsed = true;
    List<string> _emails = new List<string>();
    List<string> _ccs = new List<string>();
    public void CollapseButton()
    {
        Vector3 eulerAngles = CollapsedButton.transform.eulerAngles;
        if (_collapsed) CollapsedButton.transform.eulerAngles = new Vector3(eulerAngles.x, eulerAngles.y, 90);
        else CollapsedButton.transform.eulerAngles = new Vector3(eulerAngles.x, eulerAngles.y, -90);
        _collapsed = !_collapsed;

        ToRoot.sizeDelta = new Vector2(ToRoot.sizeDelta.x, ToSize = _collapsed ? collapsedSize : uncollapsedSize);
        CollapsedTo.SetActive(_collapsed);
        UncollapsedTo.SetActive(!_collapsed);
    }
    void ToChanged(string email)
    {
        if (string.IsNullOrEmpty(email)) return;
        if (!Util.IsEmail(email))
            Main.Instance.Notify(Settings.EMAIL_NOT_VALID);
        else if(_emails.Contains(email)) Main.Instance.Notify(Settings.EMAIL_ALREADY_ADDED);
        else
        {
            AddEmail(email, EmailType.TO);
            ToInputField.text = string.Empty;
            ToInputField.Select();
        }
    }
    void ToAdded(string email)
    {
        if (string.IsNullOrEmpty(email)) return;

        if (!Util.IsEmail(email)) Main.Instance.Notify(Settings.EMAIL_NOT_VALID);
        else if (_emails.Contains(email)) Main.Instance.Notify(Settings.EMAIL_ALREADY_ADDED);
        else
        {
            AddEmail(email, EmailType.TO);
            UncollapsedToInputField.text = string.Empty;
            UncollapsedToInputField.Select();
        }
    }
    void CcAdded(string email)
    {
        if (string.IsNullOrEmpty(email)) return;

        if (!Util.IsEmail(email)) Main.Instance.Notify(Settings.EMAIL_NOT_VALID);
        else if (_ccs.Contains(email)) Main.Instance.Notify(Settings.EMAIL_ALREADY_ADDED);
        else
        {
            AddEmail(email, EmailType.CC);
            UncollapsedCcInputField.text = string.Empty;
            UncollapsedCcInputField.Select();
        }
    }
    void AddEmail(string email, EmailType type)
    {
        switch (type)
        {
            case EmailType.TO:
                _emails.Add(email);
                CheckMail();
                Instantiate(MailPrefab, UncollapsedToRoot).GetComponent<MailText>().SetEmail(email, type);
                break;
            case EmailType.CC:
                _ccs.Add(email);
                Instantiate(MailPrefab, UncollapsedCcRoot).GetComponent<MailText>().SetEmail(email, type);
                break;
        }
        SetUncollapsedSize();
    }
    public void RemoveEmail(string email, EmailType type)
    {
        switch (type)
        {
            case EmailType.TO:
                _emails.Remove(email);
                CheckMail();
                break;
            case EmailType.CC:
                _ccs.Remove(email);
                break;
        }
        SetUncollapsedSize();
    }
    void SetUncollapsedSize()
    {
        uncollapsedSize = 144f * (1/*for Cc text*/ + 1/*to input field*/ + 1/*cc input field*/ + _emails.Count + _ccs.Count);
        UncollapsedCcRT.anchoredPosition = new Vector2(0f, -144f * (_emails.Count + 1/*to input field*/));
        if (!_collapsed)
            ToRoot.sizeDelta = new Vector2(ToRoot.sizeDelta.x, ToSize = uncollapsedSize);
        UncollapsedToRT.sizeDelta = new Vector2(UncollapsedToRT.sizeDelta.x, 144f * (1/*to input field*/ + _emails.Count));
        UncollapsedCcRT.sizeDelta = new Vector2(UncollapsedCcRT.sizeDelta.x, 144f * (1/*for Cc text*/ + 1/*cc input field*/ + _ccs.Count));
    }
    #endregion

    #region subject
    float _subjectSize = 144f;
    float SubjectSize
    {
        get { return _subjectSize; }
        set
        {
            if (_subjectSize != value)
            {
                //subjectmessageRoot .position += value - size
                MessageRoot.anchoredPosition += new Vector2(0f, _subjectSize - value);
                _subjectSize = value;
            }
        }
    }
    public RectTransform MessageRoot;
    public InputField SubjectInputField;
    public RectTransform SubjectInputFieldRectTransform;
    public Text SubjectParentText;
    public RectTransform SubjectParentTextRectTransform;
    void OnSubjectInputFieldChange(string value)
    {
        SubjectParentText.text = value;
        SubjectInputFieldRectTransform.sizeDelta = new Vector2(SubjectInputFieldRectTransform.sizeDelta.x, SubjectParentTextRectTransform.sizeDelta.y + 48 * 2);
        SubjectSize = SubjectInputFieldRectTransform.sizeDelta.y;
        CheckMail();
    }
    #endregion

    #region message
    public InputField BodyInputField;

    public InputField MessageInputField;
    public RectTransform MessageInputFieldRectTransform;
    public Text MessageParentText;
    public RectTransform MessageParentTextRectTransform;
    public Text ImageNameText;
    public Text MapImageNameText;


    void OnMessageInputFieldChange(string value)
    {
        MessageParentText.text = value;
        MessageInputFieldRectTransform.sizeDelta = new Vector2(MessageInputFieldRectTransform.sizeDelta.x, MessageParentTextRectTransform.sizeDelta.y + 48 * 2 + 410f/*image*/);
    }

    public Image ScreenshotImage;
    public Image MapImage;
    public Button ScreenshotImageButton;
    public Button MapImageButton;
    public Image ScreenshotImageTicked;
    public Image MapImageUnTicked;
    public Sprite ImageTicked;
    public Sprite ImageUnTicked;
    bool _screenshotTicked;
    bool ScreenshotTicked
    {
        get
        {
            return _screenshotTicked;
        }
        set
        {
            _screenshotTicked = value;
            ScreenshotImageTicked.sprite = _screenshotTicked ? ImageTicked : ImageUnTicked;
        }
    }
    bool _mapTicked;
    bool MapTicked
    {
        get
        {
            return _mapTicked;
        }
        set
        {
            _mapTicked = value;
            MapImageUnTicked.sprite = _mapTicked ? ImageTicked : ImageUnTicked;
        }
    }
    #endregion
    
    IEnumerator GetMapImage()
    {
        while (true)
        {
            if (Main.Instance.mapLocation.mapBytes != null) break;
            Debug.Log("MapImage: not downloaded");
            yield return new WaitForSeconds(1f);
        }
        Debug.Log("MapImage: GetMapImage start");
        Texture2D texture = new Texture2D(111, 114);
        texture.LoadImage(Main.Instance.mapLocation.mapBytes);
        Sprite mapSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(.5f, .5f), 100, 0, SpriteMeshType.FullRect);
        MapImage.sprite = mapSprite;
        Debug.Log("MapImage: GetMapImage end");
    }

    public override void OpenPopup(Dictionary<string, object> data)
    {
        if (data.ContainsKey("ScreenshotImageMobile"))
        {
            Image ScreenshotImageMobile = (Image)(data["ScreenshotImageMobile"]);
            ScreenshotImage.sprite = ScreenshotImageMobile.sprite;
            Main.Instance.StartCoroutine(GetMapImage());
        }
    }

    public override void ClosePopup()
    {

    }
}