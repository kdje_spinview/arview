﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MailText : MonoBehaviour
{
    public RectTransform ImageRect;
    public RectTransform TextRect;
    public Text Text;
    public Button RemoveButton;
    EmailType _type;


    public void SetEmail(string email, EmailType type)
    {
        Text.text = email;//this will change size
        //ImageRect.sizeDelta = new Vector2(TextRect.sizeDelta.x + 75f/*X button + padding*/, 100f);
        Debug.Log("Mailing MailText SetEmail");
        if(gameObject.activeSelf)
            StartCoroutine(UpdateImageSize());
        RemoveButton.onClick.AddListener(RemoveMe);
        _type = type;
        transform.SetSiblingIndex(transform.parent.childCount - 2);
    }

    IEnumerator UpdateImageSize()
    {
        yield return new WaitForEndOfFrame();
        ImageRect.sizeDelta = new Vector2(TextRect.sizeDelta.x + 75f/*X button + padding*/, 100f);
    }
    private void OnEnable()
    {
        Debug.Log("Mailing MailText OnEnable");
        if (gameObject.activeSelf)
            StartCoroutine(UpdateImageSize());
        //ImageRect.sizeDelta = new Vector2(TextRect.sizeDelta.x + 75f/*X button + padding*/, 100f);
    }
    private void Awake()
    {
        Debug.Log("Mailing MailText Awake");
        if (gameObject.activeSelf)
            StartCoroutine(UpdateImageSize());
    }
    void RemoveMe()
    {

        var mailPopup = (MailSystem)PopupManager.Instance.GetPopup(PopupType.MAIL);
        if (mailPopup != null) mailPopup.RemoveEmail(Text.text, _type);
        Destroy(this.gameObject);
    }
}
