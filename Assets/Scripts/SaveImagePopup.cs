﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


public class SaveImagePopup : Popup
{
    public Button SelectLibraryButton;
    public Button SelectDeviceButton;

    public Text ScreenshotImageTitle;
    public Text MapImageTitle;


    private void Awake()
    {
        SelectLibraryButton.onClick.AddListener(SelectLibrary);
        SelectDeviceButton.onClick.AddListener(SelectDevice);

        SetImageSizes();
    }
    public RectTransform RootRect;
    public RectTransform ScreenshotImageRect;
    public RectTransform MapImageRect;
    void SetImageSizes()
    {
        float width = RootRect.rect.width;
        float imageWidth = (width - 3 * 45f /*spacing*/) / 2f;
        ScreenshotImageRect.sizeDelta = MapImageRect.sizeDelta = new Vector2(imageWidth, ScreenshotImageRect.sizeDelta.y);
    }
    
    void SelectLibrary()
    {
        PopupManager.Instance.OpenPopup(PopupType.LIBRARY, new Dictionary<string, object>() { { "mode", LibraryMode.PLATFORM } });
        //StartCoroutine(SaveToPlatform());
        //https://api.spinviewglobal.com/api/v1/folders/root?_token=PdSydHvCJBCKqbvMCKiQmJliaCEiV0KDBtJKDD3lVSB2iMH5Z1
    }

    void SelectDevice()
    {
        //PopupManager.Instance.OpenPopup(PopupType.LIBRARY, new Dictionary<string, object>() { { "mode", LibraryMode.PLATFORM } });
        _screenshotPopup.SaveImageInGalery(imageTitle/*Util.GetDevice() == DeviceType.MOBILE? 
            (ImageTitleInputFieldMobile.text.EndsWith(".png") ? ImageTitleInputFieldMobile.text : ImageTitleInputFieldMobile.text + ".png") :
            (ImageTitleInputFieldTablet.text.EndsWith(".png") ? ImageTitleInputFieldTablet.text : ImageTitleInputFieldTablet.text + ".png")*/,
            ScreenshotTicked, MapTicked);

        PopupManager.Instance.ClosePopup();//ClosePopup();
        Main.Instance.Notify(Settings.IMAGE_SAVED_DEVICE);
    }

    public IEnumerator SaveToPlatformFolder(string folderID)
    {
        bool connection = false;
        yield return StartCoroutine(Main.Instance.CheckInternetConnection((isConnected) => {
            // handle connection status here
            connection = isConnected;
        }));

        if (!connection)
        {
            Main.Instance.Notify(Settings.NO_INTERNET);
            yield break;
        }

        Main.Instance.loadingBlur.Activate("saving");
        string uploadURL = "https://api.spinviewglobal.com/api/v1/sites/upload?folder-id=" + folderID;

        List<IMultipartFormSection> formData;

        string imgName = imageTitle;/* Util.GetDevice() == DeviceType.MOBILE ?
            (ImageTitleInputFieldMobile.text.EndsWith(".png") ? ImageTitleInputFieldMobile.text : ImageTitleInputFieldMobile.text + ".png") :
            (ImageTitleInputFieldTablet.text.EndsWith(".png") ? ImageTitleInputFieldTablet.text : ImageTitleInputFieldTablet.text + ".png");*/

        string mapName = "Map" + imgName;

        notificationShown = false;
        formData = new List<IMultipartFormSection>();
        if (ScreenshotTicked && _screenshotPopup.imageBytes != null)
        {
            wrCounter++;
            formData = new List<IMultipartFormSection>();
            formData.Add(new MultipartFormDataSection("name", imgName));
            formData.Add(new MultipartFormFileSection("attachment", _screenshotPopup.imageBytes, imgName, "image"));

            yield return StartCoroutine(WebRequestFunc(uploadURL, formData, Convert.ToInt32(folderID), imgName));
        }

        if (MapTicked && Main.Instance.mapLocation.mapBytes != null)
        {
            wrCounter++;
            formData = new List<IMultipartFormSection>();
            formData.Add(new MultipartFormDataSection("name", mapName));
            formData.Add(new MultipartFormFileSection("attachment", Main.Instance.mapLocation.mapBytes, mapName, "image"));

            yield return StartCoroutine(WebRequestFunc(uploadURL, formData, Convert.ToInt32(folderID), mapName));
        }
        StartCoroutine(CloseWhenDone());
    }

    public IEnumerator SaveToPlatform()
    {
        bool connection = false;
        yield return StartCoroutine(Main.Instance.CheckInternetConnection((isConnected) => {
            // handle connection status here
            connection = isConnected;
        }));

        if (!connection)
        {
            Main.Instance.Notify(Settings.NO_INTERNET);
            yield break;
        }

        if (Settings.arSSFolderID == "")
        {
            string api = "https://api.spinviewglobal.com/api/v1/folders/" + Settings.rootFolderID + "?folder-id=" + Settings.rootFolderID + "&_token=" + Settings.accessToken;
            //string api = "https://api.spinviewglobal.com/api/v1/folders/" + Settings.rootFolderID + "?_token=qLSZhfQBrHZwuPaUleMrPOrzu6WlFf66xBlAAb089krpMGZupY";
            //WWW wwww = new WWW(api);
            int token = WebRequest.GetNewToken();

            yield return WebRequest.Get(token, api);

            //yield return wwww;
            if(!WebRequest.Error(token))
            //if (wwww.error == null)
            {
                try
                {
                    Dictionary<string, List<object>> response = JsonConvert.DeserializeObject<Dictionary<string, List<object>>>(/*wwww.text*/WebRequest.StringData(token));
                    if (response.ContainsKey("data"))
                    {
                        foreach (var item in response["data"])
                        {
                            Dictionary<string, object> response2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.ToString());
                            if (response2.ContainsKey("folder") && response2["folder"] != null)
                            {
                                Dictionary<string, object> response3 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["folder"].ToString());
                                if (response3.ContainsKey("metadata") && response3["metadata"] != null)
                                {
                                    Dictionary<string, object> response4 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response3["metadata"].ToString());
                                    if (response4.ContainsKey("arapp_data") && response4["arapp_data"] != null)
                                    {
                                        if (response4["arapp_data"].ToString() == Settings.screenshotsFolder)
                                        {
                                            if (response3.ContainsKey("id") && response3["id"] != null)
                                            {
                                                Settings.arSSFolderID = response3["id"].ToString();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("SaveImagePopup>SaveToPlatform ERROR = " + e.ToString());
                }
            }
            else
            {
                Main.Instance.Notify(Settings.SOMETHING_WENT_WRONG);
                Debug.Log("SaveImagePopup>SaveToPlatform WebRequestError");
                PopupManager.Instance.ClosePopup();
                yield break;
            }
        }

        Main.Instance.loadingBlur.Activate("saving");
        string uploadURL = "https://api.spinviewglobal.com/api/v1/sites/upload?folder-id=" + Settings.arSSFolderID;

        List<IMultipartFormSection> formData;

        string imgName = imageTitle;/* Util.GetDevice() == DeviceType.MOBILE ?
            (ImageTitleInputFieldMobile.text.EndsWith(".png") ? ImageTitleInputFieldMobile.text : ImageTitleInputFieldMobile.text + ".png") :
            (ImageTitleInputFieldTablet.text.EndsWith(".png") ? ImageTitleInputFieldTablet.text : ImageTitleInputFieldTablet.text + ".png");*/


        string mapName = "Map" + imgName;

        notificationShown = false;
        formData = new List<IMultipartFormSection>();
        if (ScreenshotTicked && _screenshotPopup.imageBytes != null)
        {
            wrCounter++;
            formData = new List<IMultipartFormSection>();
            formData.Add(new MultipartFormDataSection("name", imgName));            
            formData.Add(new MultipartFormFileSection("attachment", _screenshotPopup.imageBytes, imgName, "image"));

            //yield return StartCoroutine(WebRequestFunc(uploadURL, formData));
        }

        if (MapTicked && Main.Instance.mapLocation.mapBytes != null)
        {
            wrCounter++;
            formData = new List<IMultipartFormSection>();
            formData.Add(new MultipartFormDataSection("name", mapName));            
            formData.Add(new MultipartFormFileSection("attachment", Main.Instance.mapLocation.mapBytes, mapName, "image"));

            //yield return StartCoroutine(WebRequestFunc(uploadURL, formData));
        }
        StartCoroutine(CloseWhenDone());
    }

    int wrCounter = 0;
    IEnumerator CloseWhenDone()
    {
        while(wrCounter > 0)
        {
            yield return null;
        }
        Main.Instance.loadingBlur.Deactivate();
        PopupManager.Instance.ClosePopup();
    }
    bool notificationShown = false;
    IEnumerator WebRequestFunc(string uploadURL, List<IMultipartFormSection> formData, int folderID, string imgName)
    {
        UnityWebRequest www = UnityWebRequest.Post(uploadURL, formData);
        www.SetRequestHeader("Authorization", "Bearer " + Settings.accessToken);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log("SendMessage: 7");
            if(!notificationShown) Main.Instance.Notify(Settings.SOMETHING_WENT_WRONG);
            notificationShown = true;
        }
        else
        {
            Debug.Log("SendMessage: 8");
            if (!notificationShown) Main.Instance.Notify(Settings.IMAGE_SAVED_LIBRARY);
            notificationShown = true;

            Dictionary<string, object> response1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(www.downloadHandler.text);
            if (response1.ContainsKey("image") && response1["image"] != null && response1["image"].ToString() != "")
            {
                Dictionary<string, object> response2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response1["image"].ToString());
                if (response2.ContainsKey("key") && response2["key"] != null && response2["key"].ToString() != "")
                {
                    string key = response2["key"].ToString();
                    ImageData img = new ImageData();
                    img.name = imgName;
                    img.key = key;
                    Settings._library.storageFolders[folderID].images_data.Add(img);
                }
            }
        }
        wrCounter--;
    }
    string imageTitle;
    public override void OpenPopup(Dictionary<string, object> data)
    {

        if (data.ContainsKey("filename"))
        {
            imageTitle = (string)(data["filename"]);

            /*ScreenshotImageTitleMobile.text = imageTitle;
            ScreenshotImageTitleTablet.text = imageTitle;

            MapImageTitleMobile.text = "Map" + imageTitle;
            MapImageTitleTablet.text = "Map" + imageTitle;*/

            Util.SetImageText(ScreenshotImageTitle, imageTitle, 13);
            Util.SetImageText(MapImageTitle, "Map" + imageTitle, 13);

            DeviceType deviceType = Util.GetDevice();
            Orientation orientation = Util.GetOrientation();
        }
        _screenshotPopup = (ScreenshotManager)PopupManager.Instance.GetPopup(PopupType.SCREENSHOT);
        if (_screenshotPopup == null) PopupManager.Instance.ClosePopup();

        ScreenshotImage.sprite = _screenshotPopup.ScreenshotImage.sprite;
        Main.Instance.StartCoroutine(GetMapImage());

        ScreenshotTicked = true;
        MapTicked = true;

        ScreenshotImageButto.onClick.AddListener(() => ScreenshotTicked = !ScreenshotTicked);
        MapImageButton.onClick.AddListener(() => MapTicked = !MapTicked);
    }
    IEnumerator GetMapImage()
    {
        while (true)
        {
            if (Main.Instance.mapLocation.mapBytes != null) break;
            Debug.Log("MapImage: not downloaded");
            yield return new WaitForSeconds(1f);
        }
        Debug.Log("MapImage: GetMapImage start");
        Texture2D texture = new Texture2D(111, 114);
        texture.LoadImage(Main.Instance.mapLocation.mapBytes);
        Sprite mapSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(.5f, .5f), 100, 0, SpriteMeshType.FullRect);
        
        MapImage.sprite = mapSprite;
        Debug.Log("MapImage: GetMapImage end");
    }

    ScreenshotManager _screenshotPopup;
    public override void ClosePopup()
    {
    }


    public Image ScreenshotImage;
    public Image MapImage;
    public Button ScreenshotImageButto;
    public Button MapImageButton;
    public Image ScreenshotImageTicked;
    public Image MapImageTicked;
    public Sprite ImageTicked;
    public Sprite ImageUnTicked;
     
    bool _screenshotTicked;
    bool ScreenshotTicked
    {
        get
        {
            return _screenshotTicked;
        }
        set
        {
            _screenshotTicked = value;
            ScreenshotImageTicked.sprite = _screenshotTicked ? ImageTicked : ImageUnTicked;
        }
    }
    bool _mapTicked;
    bool MapTicked
    {
        get
        {
            return _mapTicked;
        }
        set
        {
            _mapTicked = value;
            MapImageTicked.sprite = _mapTicked ? ImageTicked : ImageUnTicked;
        }
    }
}
