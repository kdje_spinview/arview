﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class FTPManager : MonoBehaviour
{
    #region SINGLETON
    private static FTPManager instance = null;

    public static FTPManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<FTPManager>();
                DontDestroyOnLoad(instance);
            }

            return instance;
        }
    }
    #endregion
    public void LoadData()
    {
        StartCoroutine(GetFolders());
    }

    public IEnumerator GetPlatformData()
    {
        yield return StartCoroutine(GetAllFolders());
        yield return StartCoroutine(GetAllImages());
        //yield return StartCoroutine(GetFolders());
        yield return StartCoroutine(GetAllModels());
    }

    IEnumerator GetAllImages()
    {
        //za prikazivanje slika pri cuvanju
        //http://api.spinviewglobal.com/api/v1/sites?_token=6wyAPykBXGxJj41Oz1v1c3Ekky9yagzdUJgP6iH09ExNZiVRgK

        string link = "http://api.spinviewglobal.com/api/v1/sites?type=image&_token=" + Settings.accessToken;
        //UnityWebRequest www = UnityWebRequest.Get(link);
        //www.chunkedTransfer = false;
        //
        //for(int i = 0; i < 5; i++)
        //{
        //    yield return www.SendWebRequest();
        //
        //    if (string.IsNullOrEmpty(www.error)) break;
        //
        //    yield return new WaitForSeconds(0.25f);
        //}

        int token = WebRequest.GetNewToken();
        yield return WebRequest.Get(token, link);
        if (!WebRequest.Error(token))
        //if (string.IsNullOrEmpty(www.error))
        {
            //Debug.Log(www.downloadHandler.text);
            Debug.Log(WebRequest.StringData(token));
            string s = WebRequest.StringData(token);
            //Dictionary<string, List<object>> response = JsonConvert.DeserializeObject<Dictionary<string, List<object>>>(s);
            Dictionary<string, object> response = JsonConvert.DeserializeObject<Dictionary<string, object>>(s);
            WebRequest.RemoveData(token);
            if (response.ContainsKey("data"))
            {
                List<object> list = JsonConvert.DeserializeObject<List<object>>(response["data"].ToString());
                foreach (var item in list)
                {
                    string imgName = "";
                    string imageUrl = "";
                    string folder_id = "";

                    Dictionary<string, object> response2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.ToString());
                    if (response2.ContainsKey("site") && response2["site"] != null && response2["site"].ToString() != "")
                    {

                        Dictionary<string, object> response3 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["site"].ToString());
                        if (response3.ContainsKey("name") && response3["name"] != null && response3["name"].ToString() != "")
                        {
                            imgName = response3["name"].ToString();
                        }

                        if (response3.ContainsKey("folder_id") && response3["folder_id"] != null && response3["folder_id"].ToString() != "")
                        {
                            folder_id = response3["folder_id"].ToString();
                        }
                    }
                    if (response2.ContainsKey("image") && response2["image"] != null && response2["image"].ToString() != "")
                    {
                        Dictionary<string, object> response5 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["image"].ToString());
                        if (response5.ContainsKey("key"))
                        {
                            imageUrl = response5["key"].ToString();
                        }
                    }

                    ImageData img = new ImageData();
                    img.name = imgName;
                    img.key = imageUrl;

                    int folderID = 0;

                    try
                    {
                        folderID = Convert.ToInt32(folder_id);
                    }
                    catch (Exception)
                    {
                        Debug.Log("bad int");
                    }

                    if (Settings._library.storageFolders.ContainsKey(folderID))
                    {
                        if (Settings._library.storageFolders[folderID].images_data == null)
                            Settings._library.storageFolders[folderID].images_data = new List<ImageData>() { img };
                        else
                            Settings._library.storageFolders[folderID].images_data.Add(img);
                    }
                }
            }
        }
    }

    IEnumerator GetAllModels()
    {
        //logic:
        //if the item is not contained in a folder that is not marked as a folder containing ar_app models, it's legit
        //then we check if the model is hidden or not, and if not, we get all the info and add it to our list
        //http://api.spinviewglobal.com/api/v1/sites?_token=6wyAPykBXGxJj41Oz1v1c3Ekky9yagzdUJgP6iH09ExNZiVRgK;

        string link = "http://api.spinviewglobal.com/api/v1/sites?_token=" + Settings.accessToken;
        int token = WebRequest.GetNewToken();
        yield return WebRequest.Get(token, link);
        if (!WebRequest.Error(token))
        //if (string.IsNullOrEmpty(www.error))
        {
            //Debug.Log(www.downloadHandler.text);
            Debug.Log(WebRequest.StringData(token));
            Dictionary<string, object> response = JsonConvert.DeserializeObject<Dictionary<string, object>>(WebRequest.StringData(token));
            WebRequest.RemoveData(token);
            if (response.ContainsKey("data"))
            {
                List<object> list = JsonConvert.DeserializeObject<List<object>>(response["data"].ToString());
                foreach (var item in list)
                {
                    string modelName = "";
                    string folderName = "";
                    string imageUrl = "";
                    string id = "";
                    string lastUpdateDate = "";
                    string type = "";
                    Vector3 scale = new Vector3(1, 1, 1);
                    Vector2 dimmensions = new Vector3(1, 1);
                    int folderid = 0;

                    Dictionary<string, object> response2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.ToString());
                    if (CheckKey(response2, "site"))
                    {
                        Dictionary<string, object> response3 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["site"].ToString());

                        //is the item in a folder that has ar_app tag
                        //if no, we dont need it
                        if (CheckKey(response3, "folder_id"))
                        {
                            folderid = Convert.ToInt32(response3["folder_id"].ToString());
                            if (!Settings._library.modelFolders.ContainsKey(folderid))
                            {
                                continue;
                            }
                        }

                        if (CheckKey(response3, "metadata"))
                        {
                            Dictionary<string, object> metadata = JsonConvert.DeserializeObject<Dictionary<string, object>>(response3["metadata"].ToString());
                            bool active = true;
                            if (metadata.ContainsKey("active"))
                            {
                                Boolean.TryParse(metadata["active"].ToString(), out active);
                            }

                            if (!active)
                            {
                                continue;
                            }

                            string dimmensionsString = "size";

                            if (metadata.ContainsKey(dimmensionsString))
                            {
                                string dim = metadata[dimmensionsString].ToString();
                                List<string> values = dim.Split(',').ToList();
                                dimmensions.x = float.Parse(values[0]);
                                dimmensions.y = float.Parse(values[1]);
                            }
                        }

                        if (CheckKey(response3, "name"))
                        {
                            modelName = response3["name"].ToString();
                        }
                        if (CheckKey(response3, "type"))
                        {
                            type = response3["type"].ToString();
                        }
                    }

                    if (Settings._library.modelFolders.ContainsKey(folderid))
                    {
                        folderName = Settings._library.modelFolders[folderid].name;
                    }
                    

                    if (CheckKey(response2, "image"))
                    {
                        Dictionary<string, object> response5 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["image"].ToString());
                        if (response5.ContainsKey("key"))
                        {
                            imageUrl = response5["key"].ToString();
                        }
                    }
                    if (CheckKey(response2, "production_version"))
                    {
                        Dictionary<string, object> response6 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["production_version"].ToString());
                        if (CheckKey(response6, "id"))
                        {
                            id = response6["id"].ToString();
                        }
                        if (CheckKey(response6, "files_updated_at"))
                        {
                            lastUpdateDate = response6["files_updated_at"].ToString();
                        }
                    }

                    if (!Settings.PlatformModels.ContainsKey(modelName))
                        Settings.PlatformModels.Add(modelName, new ModelData(modelName, Settings.GetModelType(type), folderName, imageUrl, id, lastUpdateDate, dimmensions));
                }
            }
            //if (cntr == 0)
            //    ProductPopup.Instance.SetCompaniesAndModels(Settings.PlatformModels);
            //Main.Instance.guiController.SetModelsButton(true);
        }
        Debug.Log("GetAllModels");
        //else Main.Instance.Notify(Settings.SOMETHING_WENT_WRONG);
    }

    IEnumerator GetAllFolders()
    {
        //za prikazivanje foldera pri cuvanju slika
        List<string> folderIds = new List<string>();
        Dictionary<int, FolderData> storageFolders = new Dictionary<int, FolderData>();
        Dictionary<int, FolderData> modelFolders = new Dictionary<int, FolderData>();

        //http://api.spinviewglobal.com/api/v1/folders?_token=6wyAPykBXGxJj41Oz1v1c3Ekky9yagzdUJgP6iH09ExNZiVRgK;
        string api = "http://api.spinviewglobal.com/api/v1/folders?_token=" + Settings.accessToken;

        int token = WebRequest.GetNewToken();
        yield return WebRequest.Get(token, api);

        if (!WebRequest.Error(token))
        {
            try
            {
                Dictionary<string, object> response = JsonConvert.DeserializeObject<Dictionary<string, object>>(/*wwww.text*/WebRequest.StringData(token));
                if (response.ContainsKey("data"))
                {
                    List<object> list = JsonConvert.DeserializeObject<List<object>>(response["data"].ToString());
                    foreach (var item in list)
                    {
                        Dictionary<string, object> response2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.ToString());
                        if (CheckKey(response2, "folder"))
                        {
                            bool models = false;
                            Dictionary<string, object> response3 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["folder"].ToString());
                            if (CheckKey(response3, "metadata"))
                            {
                                Dictionary<string, object> response4 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response3["metadata"].ToString());
                                if (CheckKey(response4, "arapp_data"))
                                {
                                    if (response4["arapp_data"].ToString() == Settings.folderContentType)
                                    {
                                        models = true;
                                    }
                                }
                            }


                            int parent_id = 0;
                            int folder_id = 0;
                            string name = "";
                            
                            if (CheckKey(response3, "id"))
                            {
                                folder_id = Convert.ToInt32(response3["id"].ToString());
                            }
                            if (CheckKey(response3, "name"))
                            {
                                name = response3["name"].ToString();
                            }


                            if (response2["parent_id"] == null)
                            {
                                parent_id = -1;
                            }
                            else
                            {
                                try
                                {
                                    parent_id = Convert.ToInt32(response2["parent_id"].ToString());
                                }
                                catch (Exception)
                                {
                                    Debug.Log("no parent id");
                                }
                            }
                            if (models)
                            {
                                modelFolders.Add(folder_id, new FolderData(folder_id, parent_id, name));
                            }
                            else
                            {
                                storageFolders.Add(folder_id, new FolderData(folder_id, parent_id, name));
                            }
                            
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log("FTPManager: GetModelFolders error = " + e.ToString());
            }
        }

        foreach (var item in storageFolders)
        {
            if (item.Value.parent_id != 0 || item.Value.parent_id != -1)
            {
                if (storageFolders.ContainsKey(item.Value.parent_id) && !storageFolders[item.Value.parent_id].children_ids.Contains(item.Value.folder_id))
                {
                    storageFolders[item.Value.parent_id].children_ids.Add(item.Value.folder_id);
                }
            }
        }

        foreach (var item in modelFolders)
        {
            if (item.Value.parent_id != 0 || item.Value.parent_id != -1)
            {
                if (modelFolders.ContainsKey(item.Value.parent_id) && !modelFolders[item.Value.parent_id].children_ids.Contains(item.Value.folder_id))
                {
                    modelFolders[item.Value.parent_id].children_ids.Add(item.Value.folder_id);
                }
            }
        }

        LibraryData data = new LibraryData();
        data.root_folder_id = Convert.ToInt32(Settings.rootFolderID);
        data.storageFolders = storageFolders;
        data.modelFolders = modelFolders;
        Settings._library = data;
    }

    IEnumerator GetFolders()
    {
        List<string> folderIds = new List<string>();
        //string api = "https://api.spinviewglobal.com/api/v1/folders/" + Settings.rootFolderID + "?_token=" + Settings.accessToken;
        string api = "https://api.spinviewglobal.com/api/v1/folders/" + Settings.rootFolderID + "?details=images&type=model_3d&folder-id=" + Settings.rootFolderID + "&_token=" + Settings.accessToken;
        //https://api.spinviewglobal.com/api/v1/folders/5?details=images&folder-id=5&_token=At1m266JGqJeuHgQ8MIxtpv2gp9RcCXTTuGeDAOXk1KER6ndCg
        //WWW wwww = new WWW(api);
        Settings.PlatformModels = new Dictionary<string, ModelData>();

        int token = WebRequest.GetNewToken();
        yield return WebRequest.Get(token, api);
        //yield return wwww;
        if (!WebRequest.Error(token))
        //if (wwww.error == null)
        {
            try
            {
                Dictionary<string, object> response = JsonConvert.DeserializeObject<Dictionary<string, object>>(/*wwww.text*/WebRequest.StringData(token));
                if (response.ContainsKey("data"))
                {
                    List<object> list = JsonConvert.DeserializeObject<List<object>>(response["data"].ToString());
                    foreach (var item in list)
                    {
                        Dictionary<string, object> response2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.ToString());
                        if (CheckKey(response2, "folder"))
                        {
                            Dictionary<string, object> response3 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["folder"].ToString());
                            if (CheckKey(response3, "metadata"))
                            {
                                Dictionary<string, object> response4 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response3["metadata"].ToString());
                                if (CheckKey(response4, "arapp_data"))
                                {
                                    if (response4["arapp_data"].ToString() == Settings.folderContentType)
                                    {
                                        if (response3.ContainsKey("id") && response3["id"] != null)
                                        {
                                            folderIds.Add(response3["id"].ToString());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log("FTPManager: GetModelFolders error = " + e.ToString());
            }
        }
        cntr = 0;
        foreach (var item in folderIds)
        {
            StartCoroutine(GetFolderModels(item));
        }

        Settings.StoredModels = Util.ParseDictionary(Prefs.GetJsonDataPref());

    }

    int cntr = 0;
    IEnumerator GetFolderModels(string folderID)
    {
        cntr++;
        string link = "https://api.spinviewglobal.com/api/v1/sites?details=folder&sort-by=site.name&sort-direction=asc&folder-id=" + folderID + "&_token=" + Settings.accessToken;
        //UnityWebRequest www = UnityWebRequest.Get(link);
        //www.chunkedTransfer = false;
        //
        //for(int i = 0; i < 5; i++)
        //{
        //    yield return www.SendWebRequest();
        //
        //    if (string.IsNullOrEmpty(www.error)) break;
        //
        //    yield return new WaitForSeconds(0.25f);
        //}

        int token = WebRequest.GetNewToken();
        yield return WebRequest.Get(token, link);
        if (!WebRequest.Error(token))
        //if (string.IsNullOrEmpty(www.error))
        {
            //Debug.Log(www.downloadHandler.text);
            Debug.Log(WebRequest.StringData(token));
            Dictionary<string, object> response = JsonConvert.DeserializeObject<Dictionary<string, object>>(WebRequest.StringData(token));
            WebRequest.RemoveData(token);
            if (response.ContainsKey("data"))
            {
                List<object> list = JsonConvert.DeserializeObject<List<object>>(response["data"].ToString());
                foreach (var item in list)
                {
                    string modelName = "";
                    string folderName = "";
                    string imageUrl = "";
                    string id = "";
                    string lastUpdateDate = "";
                    string type = "";
                    Vector3 scale = new Vector3(1, 1, 1);
                    Vector2 dimmensions = new Vector3(1, 1);

                    Dictionary<string, object> response2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(item.ToString());
                    if (CheckKey(response2, "site"))
                    {
                        Dictionary<string, object> response3 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["site"].ToString());
                        if (CheckKey(response3, "metadata"))
                        {
                            Dictionary<string, object> metadata = JsonConvert.DeserializeObject<Dictionary<string, object>>(response3["metadata"].ToString());
                            bool active = true;
                            if (metadata.ContainsKey("active"))
                            {
                                Boolean.TryParse(metadata["active"].ToString(), out active);
                            }

                            if (!active)
                            {
                                continue;
                            }

                            string dimmensionsString = "size";

                            if (metadata.ContainsKey(dimmensionsString))
                            {
                                string dim = metadata[dimmensionsString].ToString();
                                List<string> values = dim.Split(',').ToList();
                                dimmensions.x = float.Parse(values[0]);
                                dimmensions.y = float.Parse(values[1]);
                            }
                        }

                        if (CheckKey(response3, "name"))
                        {
                            modelName = response3["name"].ToString();
                        }
                        if (CheckKey(response3, "type"))
                        {
                            type = response3["type"].ToString();
                        }
                    }
                    if (CheckKey(response2, "folder"))
                    {
                        Dictionary<string, object> response4 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["folder"].ToString());
                        if (CheckKey(response4, "name"))
                        {
                            folderName = response4["name"].ToString();
                        }
                    }
                    if (CheckKey(response2, "image"))
                    {
                        Dictionary<string, object> response5 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["image"].ToString());
                        if (response5.ContainsKey("key"))
                        {
                            imageUrl = response5["key"].ToString();
                        }
                    }
                    if (CheckKey(response2, "production_version"))
                    {
                        Dictionary<string, object> response6 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["production_version"].ToString());
                        if (CheckKey(response6, "id"))
                        {
                            id = response6["id"].ToString();
                        }
                        if (CheckKey(response6, "files_updated_at"))
                        {
                            lastUpdateDate = response6["files_updated_at"].ToString();
                        }
                    }

                    if (!Settings.PlatformModels.ContainsKey(modelName))
                        Settings.PlatformModels.Add(modelName, new ModelData(modelName, Settings.GetModelType(type), folderName, imageUrl, id, lastUpdateDate, dimmensions));
                }
            }
            cntr--;
            //if (cntr == 0)
                //    ProductPopup.Instance.SetCompaniesAndModels(Settings.PlatformModels);
                //Main.Instance.guiController.SetModelsButton(true);
        }
        //else Main.Instance.Notify(Settings.SOMETHING_WENT_WRONG);
    }

    public IEnumerator CreateFolder(int parentId, string name)
    {
        bool connection = false;
        yield return StartCoroutine(Main.Instance.CheckInternetConnection((isConnected) =>
        {
            // handle connection status here
            connection = isConnected;
        }));

        if (!connection)
        {
            Main.Instance.Notify(Settings.NO_INTERNET);
            yield break;
        }

        int folderID = 0;

        Main.Instance.loadingBlur.Activate("saving");
        string url = "https://api.spinviewglobal.com/api/v1/folders/" + parentId;
        string json = JsonConvert.SerializeObject(new Dictionary<string, string>() { { "name", name }, { "company_id", Settings.companyID } });

        var uwr = new UnityWebRequest(url, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);
        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");
        uwr.SetRequestHeader("x-auth-token", Settings.accessToken);

        //Send the request then wait here until it returns
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
        {
            Debug.Log("SendMessage: Failed");
            Main.Instance.Notify(Settings.SOMETHING_WENT_WRONG);
        }
        else
        {
            Dictionary<string, object> response = JsonConvert.DeserializeObject<Dictionary<string, object>>(uwr.downloadHandler.text);
            if (response.ContainsKey("record"))
            {
                Dictionary<string, object> response1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response["record"].ToString());
                if (CheckKey(response1, "id"))
                {
                    folderID = Convert.ToInt32(response1["id"].ToString());
                    if (!Settings._library.storageFolders.ContainsKey(folderID))
                        Settings._library.storageFolders.Add(folderID, new FolderData(folderID, parentId, name));
                    Settings._library.storageFolders[parentId].children_ids.Add(folderID);
                    LibraryPopup.Instance.CurrentFolderId = LibraryPopup.Instance.CurrentFolderId;
                }
            }
        }
        Main.Instance.loadingBlur.Deactivate();
    }

    IEnumerator PostRequest(string url, string json)
    {
        var uwr = new UnityWebRequest(url, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);
        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");

        //Send the request then wait here until it returns
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
        {
            Debug.Log("Error While Sending: " + uwr.error);
        }
        else
        {
            Debug.Log("Received: " + uwr.downloadHandler.text);
        }
    }

    bool CheckKey(Dictionary<string, object> dic, string key)
    {
        return dic.ContainsKey(key) && dic[key] != null && dic[key].ToString() != "";
    }
}

