using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if !UNITY_WSA
using UnityEngine.XR.ARFoundation;
using UnityEngine.Experimental.XR;
#endif
using System;
using Lean.Touch;
using TriLib;

public class InteractionController : MonoBehaviour
{
    public GameObject InteractionControllerButton;
#if !UNITY_WSA
    ARPlaneManager planeManager;
    ARPointCloudManager pointManager;
    ARSessionOrigin sessionManager;
#endif

    [HideInInspector] public GameObject objectToPlace;
    public GameObject placementIndicator;
    public bool placementReady = false;
    public new Camera camera;
    public TransformParameters posTransform;

    public string modelPath = "";

    public GameObject ShadowPlanePrefab;

    public GameObject arSession;

    public void EnableLeanComponents(bool scaleLock)
    {
        if (objectToPlace == null) return;

        if (objectToPlace.GetComponent<LeanTranslate>() != null)
        {
            objectToPlace.GetComponent<LeanTranslate>().enabled = true;
            objectToPlace.GetComponent<LeanRotateCustomAxis>().enabled = scaleLock;
            objectToPlace.GetComponent<LeanScale>().enabled = !scaleLock;
        }/*
        else if (objectToPlace.transform.GetChild(0).GetComponent<LeanTranslate>() != null)
        {
            objectToPlace.transform.GetChild(0).GetComponent<LeanTranslate>().enabled = true;
            objectToPlace.transform.GetChild(0).GetComponent<LeanRotateCustomAxis>().enabled = scaleLock;
            objectToPlace.transform.GetChild(0).GetComponent<LeanScale>().enabled = !scaleLock;

        }*/
    }
    public void DisableLeanComponents()
    {
        if (objectToPlace == null) return;
        if (objectToPlace.GetComponent<LeanTranslate>() != null)
        {
            objectToPlace.GetComponent<LeanTranslate>().enabled = false;
            objectToPlace.GetComponent<LeanRotateCustomAxis>().enabled = false;
            objectToPlace.GetComponent<LeanScale>().enabled = false;
        }/*
        else if (objectToPlace.transform.GetChild(0).GetComponent<LeanTranslate>() != null)
        {
            objectToPlace.transform.GetChild(0).GetComponent<LeanTranslate>().enabled = false;
            objectToPlace.transform.GetChild(0).GetComponent<LeanRotateCustomAxis>().enabled = false;
            objectToPlace.transform.GetChild(0).GetComponent<LeanScale>().enabled = false;

        }*/
    }
#if !UNITY_WSA
    public void SetPlane(bool active)
    {
        List<ARPlane> planes = new List<ARPlane>();
        planeManager.GetAllPlanes(planes);
        
        foreach (var item in planes)
        {
            item.gameObject.SetActive(active);
        }
        

        if (active)
        {
            pointManager.enabled = active;
        }
        else
        {
            foreach (Transform item in sessionManager.trackablesParent)
            {
                if (item.gameObject.GetComponent<ParticleSystem>())
                {
                    item.gameObject.GetComponent<ParticleSystem>().Clear();
                    item.gameObject.GetComponent<ParticleSystem>().Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
                    Destroy(item);
                }
                          
            }
            pointManager.enabled = active;
        }

        planeManager.enabled = active;
    }
#endif
    public void InstantiateModel(string path)
    {
        using (var assetLoader = new AssetLoader())
        {
            var assetLoaderOptions = AssetLoaderOptions.CreateInstance();   //Creates the AssetLoaderOptions instance.
                                                                            //AssetLoaderOptions let you specify options to load your model.
                                                                            //(Optional) You can skip this object creation and it's parameter or pass null.

            //You can modify assetLoaderOptions before passing it to LoadFromFile method. You can check the AssetLoaderOptions API reference at:
            //https://ricardoreis.net/trilib/manual/html/class_tri_lib_1_1_asset_loader_options.html

            var wrapperGameObject = gameObject;                             //Sets the game object where your model will be loaded into.
                                                                            //(Optional) You can skip this object creation and it's parameter or pass null.
            objectToPlace = assetLoader.LoadFromFile(path, assetLoaderOptions, wrapperGameObject); //Loads the model synchronously and stores the reference in myGameObject.

            CheckAlbedoColor(objectToPlace);
        }
    }

    //jedan test na iphonu nije prosao, probacemo opet jednom
    public void InstantiateModelAsync(string path)
    {
        using (var assetLoaderAsync = new AssetLoaderAsync())
        {
            var assetLoaderOptions = AssetLoaderOptions.CreateInstance();   //Creates the AssetLoaderOptions instance.
                                                                            //AssetLoaderOptions let you specify options to load your model.
                                                                            //(Optional) You can skip this object creation and it's parameter or pass null.


            assetLoaderOptions.AddAssetUnloader = true;
            //You can modify assetLoaderOptions before passing it to LoadFromFile method. You can check the AssetLoaderOptions API reference at:
            //https://ricardoreis.net/trilib/manual/html/class_tri_lib_1_1_asset_loader_options.html

            var wrapperGameObject = gameObject;                             //Sets the game object where your model will be loaded into.
#if UNITY_WSA
            Debug.Log("STOP TRACKING");
            TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
#endif

            var thread = assetLoaderAsync.LoadFromFile(path, assetLoaderOptions, wrapperGameObject, delegate (GameObject myGameObject) {
                //Here you can get the reference to the loaded model using myGameObject.
                objectToPlace = myGameObject;

                CheckAlbedoColor(objectToPlace);
#if UNITY_WSA
                PlaceModel(Main.Instance.imageTarget.transform);
                Debug.Log("START TRACKING");
                TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
#else
                PlaceModel(Main.Instance.objRoot.transform);
#endif

            }); //Loads the model asynchronously and returns the reference to the created Task/Thread.

            //thread.ThreadState = System.Threading.ThreadState.
        }
    }

    private void PlaceModel(Transform root)
    {
        //InstantiateModelAsync(modelPath);
        Debug.Log("JOKAN PlaceObject");
        if (objectToPlace != null)
        {
            Info.ModelPlaced = true;

            placementReady = false;
            //objectToPlace = Instantiate(objectToPlace, placementPose.position, placementPose.rotation, Main.Instance.objRoot.transform);
            SetModel(root/*Main.Instance.objRoot.transform*/);
#if !UNITY_WSA
            SetPlane(false);
            Main.Instance.loadingBlur.Deactivate();
#endif
        }
    }
    GameObject shadowPlane = null;
    public void SetModel(Transform parent)
    {
        objectToPlace.transform.SetParent(parent/*Main.Instance.imageTarget.transform*/);
        objectToPlace.transform.localPosition = new Vector3(0, 0, 0);
        objectToPlace.transform.localRotation = Quaternion.identity;

        objectToPlace.AddComponent<LeanTranslate>();
        objectToPlace.AddComponent<LeanScale>();
        objectToPlace.AddComponent<LeanRotateCustomAxis>().Axis = Vector3.up;
        Main.Instance.ProductPlaced = true;

        if (shadowPlane == null)
        {
            shadowPlane = Instantiate(ShadowPlanePrefab, parent);
            BoxCollider bc = objectToPlace.AddComponent<BoxCollider>();
            shadowPlane.transform.localScale = new Vector3(bc.size.x / 10f * 2f, 1f, bc.size.z / 10f * 2f);
            Destroy(objectToPlace.GetComponent<BoxCollider>());
            //shadowPlane.transform.localPosition = Vector3.zero;
            //shadowPlane.transform.localRotation = Quaternion.identity;
        }
        float scale = 1;

#if !UNITY_WSA
        Transform objTransform = objectToPlace.transform;

        objTransform.position = placementPose.position;
        objTransform.rotation = placementPose.rotation;

        shadowPlane.transform.position = placementPose.position;
        shadowPlane.transform.localRotation = Quaternion.identity;
#else
        scale = 5.25f;
#endif

        if (POSFileManager.Instance.model.type == Settings.ModelType.Image)
        {
            shadowPlane.SetActive(false);
            //TO-DO
            //na osnovu markera treba ovde da skaliramo dimenzije, ove se setuju za real world za ios/android
            objectToPlace.transform.localScale = new Vector3(POSFileManager.Instance.model.dimmensions.x / 100, POSFileManager.Instance.model.dimmensions.y / 100, 1);
            objectToPlace.transform.localPosition = new Vector3(objectToPlace.transform.localPosition.x, objectToPlace.transform.localPosition.y + POSFileManager.Instance.model.dimmensions.y / 200
#if !UNITY_WSA
                - 0.5f
#endif
                ,objectToPlace.transform.localPosition.z);
            Axis.is3dModel = false;
            objectToPlace.transform.GetChild(0).gameObject.AddComponent<Ruler2d>();
            Main.Instance._measurements2d.Activate(Prefs.RulerAcive);
            //objectToPlace.transform.GetChild(0).gameObject.AddComponent<FloatingObject>();
        }
        else
        {
            shadowPlane.SetActive(true);
            //objectToPlace.transform.localScale = new Vector3(scale, scale, scale);
            Axis.is3dModel = true;
            objectToPlace.AddComponent<Ruler>();
            Main.Instance._measurements.Activate(Prefs.RulerAcive);
        }


        
        //Main.Instance._measurements.Activate(Prefs.RulerAcive);
        objectToPlace.AddComponent<FloatingObject>();
        EnableLeanComponents(Prefs.SizeLock);
        Main.Instance.guiController.Mode = AppMode.MODEL;

        Main.Instance.ActivateRulerClicked(Prefs.RulerAcive);
        placementIndicator.SetActive(false);
        CloseInfoPopupIfLast();
        InteractionControllerButton.SetActive(false);

        StartCoroutine(Scale(scale));
    }
    private void FixedUpdate()
    {
        if(shadowPlane != null && objectToPlace != null)
        {
            shadowPlane.transform.localPosition = new Vector3(objectToPlace.transform.localPosition.x, shadowPlane.transform.localPosition.y, objectToPlace.transform.localPosition.z);
        }
    }
    IEnumerator Scale(float scale)
    {
        yield return new WaitForEndOfFrame();
        objectToPlace.transform.localScale = new Vector3(objectToPlace.transform.localScale.x * scale, objectToPlace.transform.localScale.y * scale, objectToPlace.transform.localScale.z * scale);
        posTransform = new TransformParameters(objectToPlace.transform.localPosition, objectToPlace.transform.localEulerAngles, objectToPlace.transform.localScale);
    }

    void CloseInfoPopupIfLast()
    {
        Popup _infoPopup = PopupManager.Instance.GetLastPopup();
        if (_infoPopup == null) return;
        if (_infoPopup.Type == PopupType.INFO) PopupManager.Instance.ClosePopup();
    }

    void CheckAlbedoColor(GameObject model)
    {
        Transform[] allChildren = model.GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            var component = child.GetComponent<MeshRenderer>();
            if (component)
            {
                /*
                Color black = new Color(0, 0, 0);
                if (component.material.color == black)
                {
                    component.material.SetColor("_Color", Color.white);
                }*/
                component.material.SetColor("_Color", Color.white);
            }
        }
    }

#if !UNITY_WSA
    private Pose placementPose;
    public bool placementPoseIsValid = false;
    private bool tutorialStarted = false;

    /*private void Awake()
    {
        planeManager = arSession.GetComponent<ARPlaneManager>();
        pointManager = arSession.GetComponent<ARPointCloudManager>();
        sessionManager = arSession.GetComponent<ARSessionOrigin>();
    }*/

    public void SetupAR(GameObject arSession)
    {
        planeManager = arSession.GetComponent<ARPlaneManager>();
        pointManager = arSession.GetComponent<ARPointCloudManager>();
        sessionManager = arSession.GetComponent<ARSessionOrigin>();
        camera = arSession.transform.GetChild(0).gameObject.GetComponent<Camera>();
    }



    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            PlaceObject();
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            Main.Instance.UndoButton();
            Debug.Log("JOKAN IMG x = " + objectToPlace.transform.localPosition.x);
            Debug.Log("JOKAN IMG y = " + objectToPlace.transform.localPosition.y);
            Debug.Log("JOKAN IMG z = " + objectToPlace.transform.localPosition.z);
        }

        if (placementReady)
        {
            if (!ProductPopup._tutorialWatched)
            {
                if (!tutorialStarted)
                {
                    tutorialStarted = true;
                    Main.Instance.TutorialRoot.SetActive(true);
                    PopupManager.Instance.OpenPopup(PopupType.INFO, new Dictionary<string, object>() { { "message", LocalizationController.Instance.GetTranslation(Settings.SCAN_SURFACE) } });
                    //GameObject notification = Instantiate(Main.Instance.InfoPopup, Main.Instance.NotificationsRoot);
                    //notification.GetComponent<InfoPopup>().text.text = LocalizationController.Instance.GetTranslation(Settings.SCAN_SURFACE);
                    Main.Instance.StartCoroutine(Main.Instance.TurnOffTutorialInSec(5f));
                }
            }
            else
            {
                UpdatePlacementPose();
                UpdatePlacementIndicator();
            }

            //if (placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            //{
            //    PlaceObject();
            //}
        }
    }
    DateTime _prevTouch = DateTime.MinValue;//just in case
    public void Touch()
    {
        if (placementReady && placementPoseIsValid && (DateTime.Now - _prevTouch).TotalSeconds > 1f)
        {
            PlaceObject();
            _prevTouch = DateTime.Now;
        }
    }


    void PlaceObject()
    {
        if (POSFileManager.Instance.model.type == Settings.ModelType.Image)
        {
            objectToPlace = Instantiate(objectToPlace, placementPose.position, placementPose.rotation, Main.Instance.objRoot.transform);
            PlaceModel(Main.Instance.objRoot.transform);
        }
        else
        {
            Main.Instance.loadingBlur.Activate();
            InstantiateModelAsync(modelPath);
        }
    }




    private void UpdatePlacementIndicator()
    {
        if (placementReady && placementPoseIsValid)
        {
            placementIndicator.SetActive(true);
            if (!ProductPopup._productplaced)
            {
                ProductPopup._productplaced = true;
                PopupManager.Instance.OpenPopup(PopupType.INFO, new Dictionary<string, object>() { { "message", LocalizationController.Instance.GetTranslation(Settings.TAP_TO_PLACE) } });
                Main.Instance.StartCoroutine(Main.Instance.TurnOffTutorialInSec(5f));
            }
            InteractionControllerButton.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        }
        else
        {
            InteractionControllerButton.SetActive(false);
            placementIndicator.SetActive(false);
        }
        //placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
    }

    private void UpdatePlacementPose()
    {
        var screenCenter = camera.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        sessionManager.Raycast(screenCenter, hits, TrackableType.Planes);

        placementPoseIsValid = hits.Count > 0;
        if (placementPoseIsValid)
        {
            placementPose = hits[0].pose;

            var cameraForward = camera.transform.forward;
            var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
            placementPose.rotation = Quaternion.LookRotation(cameraBearing);
        }
    }

#endif

    public void FingerTapNotOverUi(LeanFinger finger)
    {
        Debug.Log("FingerTapNotOverUi");

        if (objectToPlace != null)
        {
            BoxCollider bC;
            if (POSFileManager.Instance.model.type == Settings.ModelType.Image)
            {
                bC = objectToPlace.transform.GetChild(0).gameObject.GetComponent<BoxCollider>();
            }
            else
            {
                bC = objectToPlace.GetComponent<BoxCollider>();
            }

            if (bC != null)
            {
                Camera arCamera =
#if UNITY_WSA
            Camera.main
#else
            InteractionController.Instance.camera
#endif
            ;
                Ray hit = finger.GetRay(arCamera);
                RaycastHit hitResult;
                if (bC.Raycast(hit, out hitResult, 100.0f))
                {
                    Debug.Log("BoxCollider touched");
                    //ACTIVATE MODEL EDIT MODE
                    switch (Main.Instance.guiController.Mode)
                    {
                        case AppMode.IDLE:
                            Main.Instance.guiController.Mode = AppMode.MODEL;
                            objectToPlace.GetComponent<FloatingObject>().Mode = FloatingMode.FLOATING;
                            break;
                        case AppMode.MODEL:
                            Main.Instance.guiController.Mode = AppMode.IDLE;
                            objectToPlace.GetComponent<FloatingObject>().Mode = FloatingMode.GROUND;
                            break;
                    }

                }
            }
        }
    }

#region SINGLETON
    private static InteractionController instance = null;

    public static InteractionController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<InteractionController>();
            }

            return instance;
        }
    }
#endregion

    public Camera VuforiaCamera;
    public Camera CurrentCamera
    {
        get
        {
#if UNITY_EDITOR || UNITY_IPHONE || UNITY_ANDROID
            return camera;
#else
            return VuforiaCamera;
#endif
        }
    }


    public void DestroyPlacedObject(bool buttonAction = false)
    {
        Info.ModelPlaced = false;
        Info.ModelSelected = false;

        if (objectToPlace == null) return;

        Destroy(objectToPlace);

        try
        {
            Main.Instance._measurements.Activate(false);
            Main.Instance._measurements2d.Activate(false);
#if UNITY_WSA
            foreach (Transform obj in Main.Instance.imageTarget.transform)
#else
            foreach (Transform obj in Main.Instance.objRoot.transform)
#endif
                Destroy(obj.gameObject);
        }
        catch (Exception e)
        {

        }
        objectToPlace = null;
        Resources.UnloadUnusedAssets();
        GC.Collect();

        if (buttonAction)
        {
            Main.Instance.guiController.Mode = AppMode.IDLE;
        }
#if !UNITY_WSA
        SetPlane(true);
#endif
    }
}