﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CompanyLoading : MonoBehaviour
{
    public GameObject LoadingRoot;
    public Slider Slider;

    float _currentProgress;
    float CurrentProgress
    {
        get
        {
            return _currentProgress;
        }
        set
        {
            _currentProgress = value;
            if (_currentProgress < 0) _currentProgress = 0;
            if (_currentProgress > 1) _currentProgress = 1;

            //Debug.Log("Loading: set _currentProgress = " + _currentProgress);
            //PercentText.text = ((int)(_currentProgress * 100f)).ToString();
            Slider.value = _currentProgress;
        }
    }
    public void StartLoading()
    {
        if (string.IsNullOrEmpty(Settings.companyImageKey)) return;

        //LoadingRoot.SetActive(true);
        StartCoroutine(GetCompanyImage());
    }
    void CloseLoading()
    {
        //LoadingRoot.SetActive(false);
    }
    IEnumerator GetCompanyImage()
    {
        yield return new WaitForEndOfFrame();
        RawImage CompanyRawImage = Main.Instance.CompanyRawImage;
        CompanyRawImage.gameObject.SetActive(false);
        CurrentProgress = 0.1f;
        DateTime prevTime = DateTime.Now;
        string imageUrl = "https://api.spinviewglobal.com/api/v1/images/" + Settings.companyImageKey + "?size=nullx200";// + "?size=200x200";
        
        int token = WebRequest.GetNewToken();
        
        //WWW www = new WWW(imageUrl);

        CurrentProgress = 0.25f;
        //yield return www;
        yield return WebRequest.Get(token, imageUrl, true);
        CurrentProgress = 0.33f;
        Debug.Log("DeltaTime: www download " + (DateTime.Now - prevTime).TotalMilliseconds);
        Texture2D texture2D = WebRequest.TextureData(token);
        if (!WebRequest.Error(token) && texture2D != null)
        {
            //CompanyImage.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0)/*, 100, 0, SpriteMeshType.FullRect*/);
            prevTime = DateTime.Now;
            //CompanyImage.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0), 100, 0, SpriteMeshType.FullRect);
            CompanyRawImage.texture = texture2D;
            CurrentProgress = 0.66f;
            Debug.Log("DeltaTime: CompanyImageRawImage.texture = www.texture; " + (DateTime.Now - prevTime).TotalMilliseconds);

            yield return null;

            prevTime = DateTime.Now;
            CompanyRawImage.rectTransform.sizeDelta = new Vector2(texture2D.width, texture2D.height);
            CurrentProgress = 0.8f;
            //CompanyImage.rectTransform.sizeDelta = new Vector2(www.texture.width, www.texture.height);
            Debug.Log("DeltaTime: CompanyImageRawImage.rectTransform.sizeDelta " + (DateTime.Now - prevTime).TotalMilliseconds);

            float preferedRatio = Util.GetDevice() == DeviceType.MOBILE ?
                ((22f * 145f) / (360f * 640f)) :
                ((16f * 102f) / (662f * 993f));

            float currentRatio = (texture2D.width * texture2D.height) / (Util.ScreenWidth * Util.ScreenHeight);

            //CompanyImage.transform.localScale = Vector3.one * Mathf.Sqrt(preferedRatio / currentRatio);
            CompanyRawImage.transform.localScale = Vector3.one * Mathf.Sqrt(preferedRatio / currentRatio) * 0.7f;
            //*22/640
            //CompanyImage.gameObject.SetActive(true);
            CompanyRawImage.gameObject.SetActive(true);
        }
        else
        {
            //Main.Instance.Notify(Settings.SOMETHING_WENT_WRONG);
        }
        CurrentProgress = 1f;
        yield return new WaitForSeconds(0.15f);
        CloseLoading();
    }
}
