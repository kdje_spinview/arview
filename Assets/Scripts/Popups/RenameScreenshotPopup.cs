﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using AdvancedInputFieldPlugin;

public class RenameScreenshotPopup : Popup
{
    public Button RenameButton;
    public AdvancedInputField ImageNameInputField;
    string filename;

    public Transform Root;

    bool closedOnCancel = true;

    public override void ClosePopup()
    {
        if(closedOnCancel) PopupManager.Instance.ClosePopup();//close ScreenshotPopup, too
    }

    public override void OpenPopup(Dictionary<string, object> data)
    {
        filename = string.Empty;// "IMG-" + DateTime.Now.Ticks.ToString()/* + ".png"*/;
        ImageNameInputField.Text = filename;
    }

    private void Awake()
    {
        RenameButton.onClick.AddListener(RenameButtonClicked);
        ImageNameInputField.OnValueChanged.AddListener(InputChanged);

        if (Util.GetDevice() == DeviceType.TABLET)
            Root.localScale = Vector3.one * 0.5f;
    }
    void RenameButtonClicked()
    {
        ScreenshotManager _screenshotPopup = (ScreenshotManager)PopupManager.Instance.GetPopup(PopupType.SCREENSHOT);
        if (_screenshotPopup != null)
        {
            _screenshotPopup.FileName = string.IsNullOrEmpty(ImageNameInputField.Text)? filename : ImageNameInputField.Text;
            _screenshotPopup.ActivateUI(true);
        }
        closedOnCancel = false;
        PopupManager.Instance.ClosePopup();//close this popup
    }

    void InputChanged(string value)
    {
        RenameButton.interactable = !string.IsNullOrEmpty(value);
    }
}
