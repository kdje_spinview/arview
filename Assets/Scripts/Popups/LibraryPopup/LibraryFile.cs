﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Networking;


public class LibraryFile : MonoBehaviour
{
    public Button SelectFileButton;
    public GameObject FolderImageGO;
    public GameObject SquareImageGO;
    public Image SquareImage;
    public Text FileNameText;
    public RectTransform FileNameTextRect;
    string _fileName;
    public string FileName
    {
        get
        {
            return _fileName;
        }
        set
        {
            _fileName = value;
            StartCoroutine(FixText());
            //FixText();
            //FileNameText.text = _fileName;
        }
    }
    public int FolderId { get; set; }
    #region file_image
    string _imagekey;
    public string ImageKey
    {
        get
        {
            return _imagekey;
        }
        set
        {
            _imagekey = value;
            FixForImage();
            StartCoroutine(GetImage());
        }
    }
    IEnumerator GetImage()
    {
        string imageUrl = "https://api.spinviewglobal.com/api/v1/images/" + _imagekey + "?size=64x64";

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(imageUrl);
        yield return www.SendWebRequest();
        if (string.IsNullOrEmpty(www.error))
        {
            Texture2D t2d = DownloadHandlerTexture.GetContent(www);
            if(t2d != null)
                SquareImage.sprite = Sprite.Create(t2d, new Rect(0, 0, t2d.width, t2d.height), new Vector2(0, 0));
        }

    }
    #endregion
    const float MAXTEXTWIDTH = 720f;
    IEnumerator FixText()
    {
        string fileName = _fileName;
        Util.SetTextAlpha(FileNameText, 0f);
        bool first = true;
        do
        {
            FileNameText.text = fileName + (first? "" : "...");
            first = false;
            yield return null;
            if (fileName.Length < 10) break;
            fileName = fileName.Substring(0, fileName.Length - 3);
        } while (FileNameTextRect.rect.width > MAXTEXTWIDTH);
        Util.SetTextAlpha(FileNameText, 1f);
    }

    enum FileType
    {
        FOLDER,
        IMAGE
    }
    FileType _fileType = FileType.FOLDER;

    private void Awake()
    {
        SelectFileButton.onClick.AddListener(SelectThisFile);
    }
    void SelectThisFile()
    {
        switch (_fileType)
        {
            case FileType.FOLDER:
                switch (LibraryPopup.Instance.Mode)
                {
                    case LibraryMode.LOCAL:
                        LibraryPopup.Instance.CurrentDestination += "/" + FileName;
                        LibraryPopup.Instance.LibraryName = FileName;
                        break;
                    case LibraryMode.PLATFORM:
                        LibraryPopup.Instance.CurrentFolderId = FolderId;
                        LibraryPopup.Instance.LibraryName = FileName;
                        break;
                }
                break;
            case FileType.IMAGE:
                //Do nothing
                break;
        }
    }
    public void SetText(string fileName)
    {
        FileName = fileName;
    }

    #region local
    public void SetImage(FileInfo fileInfo)
    {
        FixForImage();
        //TODO: load image 
        Texture2D tex = Util.LoadPNG(fileInfo.FullName);
        SquareImage.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
    }
    #endregion
    void FixForImage()
    {
        _fileType = FileType.IMAGE;

        Color hexGrey = new Color();
        ColorUtility.TryParseHtmlString("#8b8b8b", out hexGrey);
        FileNameText.color = hexGrey;

        SelectFileButton.interactable = false;
        FolderImageGO.SetActive(false);
        SquareImageGO.SetActive(true);
    }
    #region platform
    //probably do nothing hereeeee
    #endregion
}
