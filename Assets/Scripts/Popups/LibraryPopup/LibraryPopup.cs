﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using AdvancedInputFieldPlugin;

public enum LibraryMode
{
    LOCAL,
    PLATFORM
}
public class LibraryPopup : Popup
{
    public LibraryMode Mode { get; private set; }

    public Button NewFolderButton;
    public Button SaveButton;
    public Button BackButton;

    public Button EmptyFolderSaveButton;

    public GameObject LibraryFilePrefab;

    public Transform FilesRoot;

    public Text LibraryText;
    public RectTransform LibraryTextTRect;
    string _libraryName;
    public string LibraryName
    {
        set
        {
            _libraryName = value;
            StartCoroutine(FixText()); //LibraryText.text = _libraryName;
        }
    }
    const float MAXTEXTWIDTH = 720f;
    IEnumerator FixText()
    {
        Util.SetTextAlpha(LibraryText, 0f);
        string libraryName = _libraryName;
        bool first = true;
        do
        {
            LibraryText.text = libraryName + (first ? "" : "...");
            first = false;
            yield return null;
            if (libraryName.Length < 10) break;
            libraryName = libraryName.Substring(0, libraryName.Length - 3);
        } while (LibraryTextTRect.rect.width > MAXTEXTWIDTH);
        Util.SetTextAlpha(LibraryText, 1f);
    }

    public AdvancedInputField SearchInputField;


    private void Awake()
    {
        NewFolderButton.onClick.AddListener(() => PopupManager.Instance.OpenPopup(PopupType.CREATEFOLDER));
        SaveButton.onClick.AddListener(Save);
        BackButton.onClick.AddListener(GoBack);

        EmptyFolderSaveButton.onClick.AddListener(Save);

        SearchInputField.OnValueChanged.AddListener(SearchValueChanged);
    }
    public void CreateNewFolder(string folderName)
    {
        switch (Mode)
        {
            case LibraryMode.LOCAL:
                //create new folder on current path
                DirectoryInfo di = Directory.CreateDirectory(CurrentDestination + "/" + folderName);
                //refresh files in root
                CurrentDestination = CurrentDestination;
                break;
            case LibraryMode.PLATFORM:
                //TODO: create new folder in current folder on PLATFORM
                StartCoroutine(FTPManager.Instance.CreateFolder(CurrentFolderId, folderName));
                //refresh files in root
                //CurrentFolderId = CurrentFolderId;
                break;
        }
    }
    void Save()
    {
        //TODO: Save image(s) on current destination path
        var saveImagePopup = (SaveImagePopup)PopupManager.Instance.GetPopup(PopupType.SAVEIMAGE);
        if (saveImagePopup != null)
        {
            StartCoroutine(saveImagePopup.SaveToPlatformFolder(CurrentFolderId.ToString()));
            //TODO: Notify
            //TODO: Close Popup
        }
    }
    void GoBack()
    {
        //Go one step up in file path
        switch (Mode)
        {
            case LibraryMode.LOCAL:
                int index = CurrentDestination.LastIndexOf("/");
                if (index > 0)
                {
                    CurrentDestination = CurrentDestination.Substring(0, index); // or index + 1 to keep slash

                    index = CurrentDestination.LastIndexOf("/");
                    LibraryName = CurrentDestination.Substring(index + 1);
                }
                else PopupManager.Instance.ClosePopup();
                break;
            case LibraryMode.PLATFORM:
#if UNITY_EDITOR
                if (Settings._library == null)
                {
                    PopupManager.Instance.ClosePopup();
                    return;
                }
#endif
                if (!Settings._library.storageFolders.ContainsKey(CurrentFolderId))
                {
                    PopupManager.Instance.ClosePopup();
                    return;
                }

                int parent_id = Settings._library.storageFolders[CurrentFolderId].parent_id;
                if (parent_id != 0)
                {
                    if (!Settings._library.storageFolders.ContainsKey(parent_id))
                    {
                        PopupManager.Instance.ClosePopup();
                        return;
                    }

                    CurrentFolderId = parent_id;
                    LibraryName = Settings._library.storageFolders[CurrentFolderId].name;
                }
                else PopupManager.Instance.ClosePopup();
                break;
        }
    }

    void SearchValueChanged(string value)
    {
        foreach (var fileGO in _instantiatedFiles)
        {
            LibraryFile file = fileGO.GetComponent<LibraryFile>();
            file.gameObject.SetActive(file.FileName.ToLower().Contains(value.ToLower()));
        }
    }
    List<GameObject> _instantiatedFiles = new List<GameObject>();
    string _currentDestination;
    public string CurrentDestination
    {
        get
        {
            return _currentDestination;
        }
        set
        {
            _currentDestination = value;

            foreach (GameObject file in _instantiatedFiles)
                Destroy(file);
            _instantiatedFiles.Clear();
            //TODO: refresh files in root:
            InstantiateLocalFiles();

            ActivateEmptyFolder(_instantiatedFiles.Count == 0);
        }
    }

    int _currentFolderId;
    public int CurrentFolderId
    {
        get
        {
            return _currentFolderId;
        }
        set
        {
            _currentFolderId = value;

            foreach (GameObject file in _instantiatedFiles)
                Destroy(file);
            _instantiatedFiles.Clear();
            //TODO: refresh files in root:
            InstantiatePlatformFiles();

            ActivateEmptyFolder(_instantiatedFiles.Count == 0);
        }
    }
    #region instantiate_local_files
    void InstantiateLocalFiles()
    {
        var info = new DirectoryInfo(_currentDestination);
        var directoriesInfo = info.GetDirectories();
        foreach (DirectoryInfo directoryInfo in directoriesInfo)
            InstantiateFolderLocal(directoryInfo.Name);

        var filesInfo = info.GetFiles();
        foreach (FileInfo fileInfo in filesInfo)
            InstantiateFileLocal(fileInfo);
    }
    void InstantiateFolderLocal(string name)
    {
        GameObject file = Instantiate(LibraryFilePrefab, FilesRoot);
        _instantiatedFiles.Add(file);
        file.GetComponent<LibraryFile>().SetText(name);
    }
    void InstantiateFileLocal(FileInfo fileInfo)
    {
        if (fileInfo.Extension.ToLower() == ".png" || fileInfo.Extension.ToLower() == ".jpg")
        {
            GameObject file = Instantiate(LibraryFilePrefab, FilesRoot);
            _instantiatedFiles.Add(file);
            LibraryFile libraryFile = file.GetComponent<LibraryFile>();
            libraryFile.SetText(fileInfo.Name);
            libraryFile.SetImage(fileInfo);
        }
    }
    #endregion
    #region instantiate_platform_files
    void InstantiatePlatformFiles()
    {
        if (!Settings._library.storageFolders.ContainsKey(_currentFolderId)) return;
        //TODO: instantiate folders
        var currentFolder = Settings._library.storageFolders[_currentFolderId];
        if (currentFolder.children_ids != null)
        {
            var directoriesInfo = currentFolder.children_ids;
            foreach (int folderId in directoriesInfo)
                InstantiateFolderPlatform(folderId);
        }
        if (currentFolder.images_data != null)
        {
            var filesInfo = currentFolder.images_data;
            foreach (ImageData fileInfo in filesInfo)
                InstantiateFilePlatform(fileInfo);
        }
    }
    void InstantiateFolderPlatform(int folderId)
    {
        if (!Settings._library.storageFolders.ContainsKey(folderId)) return;// bad data

        string folderName = Settings._library.storageFolders[folderId].name;

        GameObject file = Instantiate(LibraryFilePrefab, FilesRoot);
        _instantiatedFiles.Add(file);
        file.GetComponent<LibraryFile>().SetText(folderName);
        file.GetComponent<LibraryFile>().FolderId = folderId;

    }
    void InstantiateFilePlatform(ImageData fileInfo)
    {
        GameObject file = Instantiate(LibraryFilePrefab, FilesRoot);
        _instantiatedFiles.Add(file);
        file.GetComponent<LibraryFile>().SetText(fileInfo.name);
        file.GetComponent<LibraryFile>().ImageKey = fileInfo.key;
    }
    #endregion
    public static LibraryPopup Instance;
    public override void OpenPopup(Dictionary<string, object> data)
    {
        //Debug.Log("LibraryPopup>OpenPopup>dataPath=" + Application.dataPath);
        //Debug.Log("LibraryPopup>OpenPopup>persistentDataPath=" + Application.persistentDataPath);
        //Debug.Log("LibraryPopup>OpenPopup>streamingAssetsPath=" + Application.streamingAssetsPath);
        //Debug.Log("LibraryPopup>OpenPopup>temporaryCachePath=" + Application.temporaryCachePath);
        //TODO: Go to app destination
        //CurrentDestination = Application.dataPath;
        if (data != null && data.ContainsKey("mode"))
        {
            Mode = (LibraryMode)data["mode"];
        }
        switch (Mode)
        {
            case LibraryMode.LOCAL:
                CurrentDestination = Application.persistentDataPath;
                break;
            case LibraryMode.PLATFORM:
#if UNITY_EDITOR
                if(Settings._library != null)
#endif
                CurrentFolderId = Settings._library.root_folder_id;
                break;
        }
        Instance = this;
    }
    public override void ClosePopup()
    {
        Instance = null;
    }

    #region file_exists
    public bool FolderExistsLocal(string folderName)
    {
        var info = new DirectoryInfo(_currentDestination);
        var directoriesInfo = info.GetDirectories();
        foreach (var directory in directoriesInfo)
            if (directory.Name == folderName)
                return true;
        return false;
    }
    public bool FolderExistsPlatform(string folderName)
    {
        List<int> childrenFolders = Settings._library.storageFolders[CurrentFolderId].children_ids;
        foreach (int folderId in childrenFolders)
            if (Settings._library.storageFolders[CurrentFolderId].name == folderName)
                return true;
        return false;
    }
    #endregion

    #region empty_folder
    public GameObject EmptyFolderRoot;

    void ActivateEmptyFolder(bool emptyFolder)
    {
        foreach (Transform transform in FilesRoot)
            transform.gameObject.SetActive(!emptyFolder);
        EmptyFolderRoot.SetActive(emptyFolder);
    }
    #endregion
}
