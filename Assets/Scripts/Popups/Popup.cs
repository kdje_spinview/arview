using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum PopupType
{
    SETTINGS,
    MAIL,
    SAVEIMAGE,
    SCREENSHOT,
    SCREENSHOTTABLET,
    PRODUCT,
    INFO,
    RENAMESCREENSHOT,
    UPDATEPOPUP,
    LIBRARY,
    CREATEFOLDER
}
public enum StartAnimationType
{
    SCALEIN,
    ENTERHOR,
    ENTERVER,
    CUSTOM  //must be declared in popup
}
public enum EndAnimationType
{
    SCALEDOWN,
    EXITHOR,
    EXITVER,
    CUSTOM  //must be declared in popup
}
public abstract class Popup : MonoBehaviour
{
    public List<Button> CloseButtons;
    
    [HideInInspector] public PopupType Type;

    public abstract void OpenPopup(Dictionary<string, object> data);
    public abstract void ClosePopup();

    #region animation
    public Transform AnimationRoot;
    float _popupScale;
    float GetPopupScale()
    {
        switch (Type)
        {
            case PopupType.MAIL:
            case PopupType.SAVEIMAGE:
            case PopupType.PRODUCT:
            case PopupType.INFO:
            case PopupType.RENAMESCREENSHOT:
            case PopupType.LIBRARY:
            case PopupType.CREATEFOLDER:
                if (Util.GetDevice() == DeviceType.TABLET)
                    return 0.5f;
                else return 1f;
            default:
                return 1f;
        }
    }
    public IEnumerator PlayStartAnimation()
    {
        if(FadingImage != null) StartCoroutine(FadeIn());
        float startTime = Time.time;
        switch (GetStartAnimationType(Type))
        {
            case StartAnimationType.SCALEIN:
                while (startTime + STARTANIMDURATION > Time.time)
                {
                    _popupScale = GetPopupScale();
                    AnimationRoot.localScale = Vector3.one * Mathf.SmoothStep(0f, _popupScale, (Time.time - startTime) / ENDANIMDURATION);
                    //AnimationRoot.localScale = Vector3.one * Mathf.Lerp(0f, 1f, (Time.time - startTime) / STARTANIMDURATION);
                    yield return null;
                }
                AnimationRoot.localScale = Vector3.one * _popupScale;
                break;
            case StartAnimationType.ENTERHOR:
                if (!StartAnimTransform || !EndAnimTransform) break; 
                while (startTime + STARTANIMDURATION > Time.time)
                {

                    AnimationRoot.position = new Vector3(Mathf.SmoothStep(StartAnimTransform.position.x, EndAnimTransform.position.x, (Time.time - startTime) / STARTANIMDURATION), AnimationRoot.position.y, AnimationRoot.position.z);
                    //AnimationRoot.position = Vector3.Slerp(StartAnimTransform.position, EndAnimTransform.position, (Time.time - startTime) / STARTANIMDURATION);
                    yield return null;
                }
                AnimationRoot.position = EndAnimTransform.position;
                break;
            case StartAnimationType.ENTERVER:
                if (!StartAnimTransform || !EndAnimTransform) break;
                while (startTime + STARTANIMDURATION > Time.time)
                {

                    AnimationRoot.position = new Vector3(AnimationRoot.position.x, Mathf.SmoothStep(StartAnimTransform.position.y, EndAnimTransform.position.y, (Time.time - startTime) / STARTANIMDURATION), AnimationRoot.position.z);
                    //AnimationRoot.position = Vector3.Slerp(StartAnimTransform.position, EndAnimTransform.position, (Time.time - startTime) / STARTANIMDURATION);
                    yield return null;
                }
                AnimationRoot.position = EndAnimTransform.position;
                break;
            case StartAnimationType.CUSTOM:
                yield return CustomStartAnimation();
                break;
        }
        yield break;
    }
    public Image FadingImage;
    float _startAlpha;//for fading
    public Transform StartAnimTransform;
    public Transform EndAnimTransform;

    protected const float STARTANIMDURATION = 0.2f;
    protected const float ENDANIMDURATION = 0.2f;
    public IEnumerator PlayEndAnimation(Action OnEnd)
    {
        float startTime = Time.time;
        if (FadingImage != null) StartCoroutine(FadeOut());
        switch (GetEndAnimationType(Type))
        {
            case EndAnimationType.SCALEDOWN:
                while (startTime + STARTANIMDURATION > Time.time)
                {
                    AnimationRoot.localScale = Vector3.one * Mathf.SmoothStep(_popupScale, 0f, (Time.time - startTime) / ENDANIMDURATION);
                    //AnimationRoot.localScale = Vector3.one * Mathf.Lerp(1f, 0f, (Time.time - startTime) / ENDANIMDURATION);
                    yield return null;
                }
                AnimationRoot.localScale = Vector3.zero;
                break;
            case EndAnimationType.EXITHOR:
                if (!EndAnimTransform || !StartAnimTransform) break;
                while (startTime + STARTANIMDURATION > Time.time)
                {
                    AnimationRoot.position = new Vector3(Mathf.SmoothStep(EndAnimTransform.position.x, StartAnimTransform.position.x, (Time.time - startTime) / STARTANIMDURATION), AnimationRoot.position.y, AnimationRoot.position.z);
                    //AnimationRoot.position = Vector3.Slerp(EndAnimTransform.position, StartAnimTransform.position, (Time.time - startTime) / STARTANIMDURATION);
                    yield return null;
                }
                AnimationRoot.position = StartAnimTransform.position;
                break;
            case EndAnimationType.EXITVER:
                if (!EndAnimTransform || !StartAnimTransform) break;
                while (startTime + STARTANIMDURATION > Time.time)
                {
                    AnimationRoot.position = new Vector3(AnimationRoot.position.x, Mathf.SmoothStep(EndAnimTransform.position.y, StartAnimTransform.position.y, (Time.time - startTime) / STARTANIMDURATION), AnimationRoot.position.z);
                    //AnimationRoot.position = Vector3.Slerp(EndAnimTransform.position, StartAnimTransform.position, (Time.time - startTime) / STARTANIMDURATION);
                    yield return null;
                }
                AnimationRoot.position = StartAnimTransform.position;
                break;
            case EndAnimationType.CUSTOM:
                yield return CustomEndAnimation();
                break;
        }
        OnEnd();
        yield break;
    }
    static StartAnimationType GetStartAnimationType(PopupType popupType)
    {
        DeviceType deviceType = Util.GetDevice();
        switch (popupType)
        {
            case PopupType.SETTINGS:
                return StartAnimationType.ENTERHOR;
            case PopupType.PRODUCT:
                return StartAnimationType.ENTERVER;
            case PopupType.MAIL:
            case PopupType.LIBRARY:
                if (deviceType == DeviceType.MOBILE)
                    return StartAnimationType.ENTERHOR;
                else
                    return StartAnimationType.SCALEIN;
            case PopupType.SAVEIMAGE:
            case PopupType.UPDATEPOPUP:
                if (deviceType == DeviceType.MOBILE)
                    return StartAnimationType.ENTERVER;
                else
                    return StartAnimationType.SCALEIN;
            case PopupType.INFO:
            case PopupType.RENAMESCREENSHOT:
            case PopupType.CREATEFOLDER:
                return StartAnimationType.SCALEIN;
            case PopupType.SCREENSHOT:
            default:
                return StartAnimationType.CUSTOM;
        }
    }
    static EndAnimationType GetEndAnimationType(PopupType popupType)
    {
        DeviceType deviceType = Util.GetDevice();
        switch (popupType)
        {
            case PopupType.SETTINGS:
                return EndAnimationType.EXITHOR;
            case PopupType.PRODUCT:
                return EndAnimationType.EXITVER;
            case PopupType.MAIL:
            case PopupType.LIBRARY:
                if (deviceType == DeviceType.MOBILE)
                    return EndAnimationType.EXITHOR;
                else
                    return EndAnimationType.SCALEDOWN;
            case PopupType.SAVEIMAGE:
            case PopupType.UPDATEPOPUP:
                if (deviceType == DeviceType.MOBILE)
                    return EndAnimationType.EXITVER;
                else
                    return EndAnimationType.SCALEDOWN;
            case PopupType.INFO:
            case PopupType.RENAMESCREENSHOT:
            case PopupType.CREATEFOLDER:
                return EndAnimationType.SCALEDOWN;
            case PopupType.SCREENSHOT:
            default:
                return EndAnimationType.CUSTOM;
        }
    }

    virtual protected IEnumerator CustomStartAnimation()
    {
        yield return null;
    }
    virtual protected IEnumerator CustomEndAnimation()
    {
        yield return null;
    }
    #endregion

    #region fading

    IEnumerator FadeIn()
    {
        _startAlpha = FadingImage.color.a;
        yield return Fade(0f, _startAlpha);
    }
    IEnumerator FadeOut()
    {
        yield return Fade(_startAlpha, 0f);
    }
    IEnumerator Fade(float startAlpha, float endAlpha)
    {
        float startTime = Time.time;
        while (startTime + STARTANIMDURATION > Time.time)
        {
            SetAlpha(FadingImage,
               Mathf.SmoothStep(startAlpha, endAlpha, (Time.time - startTime) / STARTANIMDURATION));
            yield return null;
        }
    }
    void SetAlpha(Image image, float alpha)
    {
        Color imageColor = image.color;
        imageColor.a = alpha;
        image.color = imageColor;
    }
    #endregion
}
