using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupManager : MonoBehaviour
{
    public Transform PopupRoot;

    #region singletone
    public static PopupManager Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    #region popups
    //prefabs
    public GameObject SettingsPopup;
    public GameObject SettingsPopupTablet;
    public GameObject MailingPopupMobile;
    public GameObject MailingPopupTablet;
    public GameObject SaveImagePopup;
    public GameObject SaveImagePopupTablet;
    public GameObject ScreenshotPopup;
    public GameObject ScreenshotTablet;
    public GameObject ProductPopup;
    public GameObject InfoPopup;
    public GameObject RenameScreenshotPopup;
    public GameObject UpdatePopup;
    public GameObject UpdatePopupTablet;
    public GameObject LibraryPopup;
    public GameObject LibraryPopupTablet;
    public GameObject CreateNewFolderPopup;
    #endregion


    public Stack<Popup> _popups = new Stack<Popup>();
    public void OpenPopup(PopupType popupType, Dictionary<string, object> data = null)
    {
        Debug.Log("PopupManager>OpenPopup-" + popupType.ToString());
        GameObject popupGO = null;
        switch (popupType)
        {
            case PopupType.SETTINGS:
                switch (Util.GetDevice())
                {
                    case DeviceType.MOBILE:
                        popupGO = Instantiate(SettingsPopup, PopupRoot);
                        break;
                    case DeviceType.TABLET:
                        popupGO = Instantiate(SettingsPopupTablet, PopupRoot);
                        break;
                }
                break;
            case PopupType.MAIL:
                switch (Util.GetDevice())
                {
                    case DeviceType.MOBILE:
                        popupGO = Instantiate(MailingPopupMobile, PopupRoot);
                        break;
                    case DeviceType.TABLET:
                        popupGO = Instantiate(MailingPopupTablet, PopupRoot);
                        break;
                }
                break;
            case PopupType.SAVEIMAGE:
                switch (Util.GetDevice())
                {
                    case DeviceType.MOBILE:
                        popupGO = Instantiate(SaveImagePopup, PopupRoot);
                        break;
                    case DeviceType.TABLET:
                        popupGO = Instantiate(SaveImagePopupTablet, PopupRoot);
                        break;
                }
                break;
            case PopupType.SCREENSHOT:
                switch (Util.GetDevice())
                {
                    case DeviceType.MOBILE:
                        popupGO = Instantiate(ScreenshotPopup, PopupRoot);
                        break;
                    case DeviceType.TABLET:
                        popupGO = Instantiate(ScreenshotTablet, PopupRoot);
                        break;
                }
                break;
            case PopupType.PRODUCT:
                popupGO = Instantiate(ProductPopup, PopupRoot);
                break;
            case PopupType.INFO:
                popupGO = Instantiate(InfoPopup, PopupRoot);
                break;
            case PopupType.RENAMESCREENSHOT:
                popupGO = Instantiate(RenameScreenshotPopup, PopupRoot);
                break;
            case PopupType.UPDATEPOPUP:
                switch (Util.GetDevice())
                {
                    case DeviceType.MOBILE:
                        popupGO = Instantiate(UpdatePopup, PopupRoot);
                        break;
                    case DeviceType.TABLET:
                        popupGO = Instantiate(UpdatePopupTablet, PopupRoot);
                        break;
                }
                break;
            case PopupType.LIBRARY:
                switch (Util.GetDevice())
                {
                    case DeviceType.MOBILE:
                        popupGO = Instantiate(LibraryPopup, PopupRoot);
                        break;
                    case DeviceType.TABLET:
                        popupGO = Instantiate(LibraryPopupTablet, PopupRoot);
                        break;
                }
                break;
            case PopupType.CREATEFOLDER:
                popupGO = Instantiate(CreateNewFolderPopup, PopupRoot);
                break;
        }

        if(popupGO == null)
        {
            Debug.Log("PopupManager> popup null");
            return;
        }

        Popup popup = popupGO.GetComponent<Popup>();
        popup.Type = popupType;
        if (popup.CloseButtons != null)
            foreach (Button button in popup.CloseButtons)
                if (button != null)
                    button.onClick.AddListener(ClosePopup);

        _popups.Push(popup);
        popup.OpenPopup(data);

        if (popupType != PopupType.SCREENSHOT && popupType != PopupType.SCREENSHOTTABLET) StartCoroutine(popup.PlayStartAnimation());
    }
    public void ClosePopup()
    {
        if(_popups.Count  == 0)
        {
            Debug.Log("PopupManager>ClosePopup empty queue");
            return;
        }

        Popup popup = _popups.Pop();
        popup.ClosePopup();
        StartCoroutine(popup.PlayEndAnimation(() =>
        {
            try
            {
                Destroy(popup.gameObject);
            } catch(Exception e)
            {
                //Debug.Log("Close popup error e = " + e);
            }
        }));        
    }

    public Popup GetPopup(PopupType popupType)
    {
        foreach (Popup popup in _popups)
            if (popupType == popup.Type)
                return popup;
        return null;
    }
    public Popup GetLastPopup()
    {
        if (_popups.Count == 0) return null;
        return _popups.Peek();
    }
    public void ClosePopupIfLast(PopupType popupType)
    {
        if (_popups.Count == 0) return;
        Popup _lastPopup = _popups.Peek();
        if (_lastPopup.Type == popupType) ClosePopup();
    }
    public bool AnyPopupOpened()
    {
        return (_popups.Count > 0);
    }
}
