﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MailSelected : MonoBehaviour
{

    public Transform Mail;
    public Text MaisText;
    public Button CloseButton;
    public Button DeleteButton;
    public HorizontalLayoutGroup Layout;

    private void Awake()
    {
        CloseButton.onClick.AddListener(() => gameObject.SetActive(false));
        DeleteButton.onClick.AddListener(DeleteButtonClicked);
    }

    void DeleteButtonClicked()
    {
        if(_currentRoot != null)
        {
            _currentRoot.DeleteMail(MaisText.text);
        }
        gameObject.SetActive(false);
    }
    EmailsRoot _currentRoot;
    public void SetMailsPosition(MailItem mailItem, EmailsRoot Root)
    {
        Mail.position = mailItem.transform.position;
        MaisText.text = mailItem.MailText.text;
        _currentRoot = Root;
        StartCoroutine(SetSpacing());
    }

    IEnumerator SetSpacing()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        if (Util.GetDevice() == DeviceType.MOBILE)
            CheckOutOfScreen();

        //Layout.spacing = MaisText.text.Length > 18 ? 0f : 310f - MaisText.GetComponent<RectTransform>().rect.width - DeleteButton.GetComponent<RectTransform>().rect.width * DeleteButton.transform.localScale.x - 50f;

        Mail.gameObject.SetActive(true);
    }
    void CheckOutOfScreen()
    {
        RectTransform mailRectTransform = Mail.GetComponent<RectTransform>();
        Vector2 mailPosition = mailRectTransform.anchoredPosition;

        //Debug.Log("CheckOutOfScreen " + GetComponent<RectTransform>().rect.width + ", " + mailPosition.x + ", " + mailRectTransform.rect.width);
        if(GetComponent<RectTransform>().rect.width < mailPosition.x + mailRectTransform.rect.width)
        {
            mailRectTransform.anchoredPosition = new Vector2(GetComponent<RectTransform>().rect.width - mailRectTransform.rect.width, mailPosition.y);
        }
    }
}
