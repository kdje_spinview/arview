﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MailItem : MonoBehaviour
{
    public RectTransform Rect;
    public Image Background;
    public Text MailText;
    public Button Button;
    public Transform lastLetterTransform;


    [HideInInspector] public EmailsRoot Root;
    bool _created = false;
    public bool Created
    {
        get
        {
            return _created;
        }
        set
        {
            Background.color = value ? Color.white : new Color(0f, 0f, 0f, 0f);
            _created = value;

            //StartCoroutine(RefreshAfterOneFrame());
        }
    }

    IEnumerator RefreshAfterOneFrame()
    {
        yield return new WaitForSeconds(0.2f);

        Rect.GetComponent<RectTransform>().ForceUpdateRectTransforms();
    }
    private void Awake()
    {
        Button.onClick.AddListener(MailClicked);
    }
    void MailClicked()
    {
        Vector2 anchPos = Rect.anchoredPosition;
        Debug.Log(anchPos);


        MailPopup _mailPopup = (MailPopup)PopupManager.Instance.GetPopup(PopupType.MAIL);
        if (_mailPopup != null && !string.IsNullOrEmpty(MailText.text))
            _mailPopup.SelectMail(this, Root);
    }
    public float Width
    {
        get
        {
            return Rect.sizeDelta.x;
        }
    }
    float Height
    {
        get
        {
            return Rect.sizeDelta.y;
        }
    }
    public Vector2 Position
    {
        get
        {
            return Rect.anchoredPosition;
        }
        set
        {
            Rect.anchoredPosition = value;
        }
    }
    public Vector2 RightDown
    {
        get
        {
            return Position + new Vector2(Width, Height);
        }
    }
    public float MostRightPoint
    {
        get
        {
            return Position.x + Width;
        }
    }
}
