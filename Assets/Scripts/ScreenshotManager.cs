using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ScreenshotManager : Popup
{
    public GameObject Root;
    
    public Image ScreenshotImage;
    public Button SaveScreenshotButton;
    public Button DontSaveScreenshotButton;
    public Button EMailButton;


    #region screenshot_data
    //data used between taking screenshot and saving image to file system
    [HideInInspector] public byte[] imageBytes;
    //private Orientation _firstOrientation;
    #endregion

    
    private void Awake()
    {
        SaveScreenshotButton.onClick.AddListener(() => SaveButtonClicked());
        EMailButton.onClick.AddListener(() => EMail());
    }

    public static bool Capturing = false;
    //[HideInInspector] public string emailScreenshotFilename = null;
    //[HideInInspector] public string emailMapScreenshotFilename = null;
    public IEnumerator CaptureScreenshot()
    {
        Main.Instance.mapLocation.mapBytes = null;
        imageBytes = null;

        Main.Instance.guiController.transform.localScale = Vector3.one * 0.001f;// Main.Instance.guiController.gameObject.SetActive(false);
        Debug.Log("JOKAN capturescreenshot");
        yield return new WaitForEndOfFrame();

        //emailScreenshotFilename = "/" + DateTime.Now.Ticks.ToString() + ".png";
        //emailMapScreenshotFilename = "/Map" + DateTime.Now.Ticks.ToString() + ".png";

        Main.Instance.mapLocation.mapBytes = null;

        Main.Instance.mapLocation.CreateMapImage();
        Capturing = true;
        Texture2D texture = null;
        yield return StartCoroutine(SaveScreenshotRoutine(result => texture = result));
        Capturing = false;
        imageBytes = texture.EncodeToPNG();

        //google camera effect
        Main.Instance.screenshotAnimation.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        Main.Instance.screenshotAnimation.SetActive(false);

        Main.Instance.guiController.transform.localScale = Vector3.one; //Main.Instance.guiController.gameObject.SetActive(true);

        ScreenshotImage.sprite = Sprite.Create(texture, new Rect(0, 0, Util.ScreenWidth, Util.ScreenHeight), new Vector2(.5f, .5f), 100, 0, SpriteMeshType.FullRect);

        //ScreenshotImageMobile.color = ScreenshotImageTablet.color = Color.white;
        Root.SetActive(true);

        //ScreenshotImageMobile.GetComponent<RectTransform>().sizeDelta = new Vector2(Util.ScreenWidth, Util.ScreenHeight);
        //OrientationHandler.Instance.SubscribeOrientationChanged(OrientationChanged);
        //*SetScreenshotRoots(_currentScreenOrientation);
        //StartCoroutine(UpdateOrientation());
        //File.Delete(emailMapScreenshotFilename);

        ActivateUI(false);
        PopupManager.Instance.OpenPopup(PopupType.RENAMESCREENSHOT);
    }
    public GameObject UiRoot;
    public void ActivateUI(bool activate)
    {
        if (activate) StartCoroutine(PlayStartAnimation());
        UiRoot.SetActive(activate);
    }
    /*void OrientationChanged(Orientation newOrientation)
    {
        ScreenshotImageMobile.transform.localScale = Vector3.one * (
                (newOrientation == _firstOrientation) ? 1f : Util.ScreenSmallerSize / Util.ScreenBiggerSize);
    }*/
    /*void SetScreenshotRoots(Orientation orientation)
    {
        switch (orientation)
        {
            case Orientation.PORTRAIT:
                //set landscape
                ScreenshotImageMobile.GetComponent<RectTransform>().sizeDelta = ScreenshotImageTablet.GetComponent<RectTransform>().sizeDelta = new Vector2(Util.ScreenWidth, Util.ScreenHeight);
                break;
            case Orientation.LANDSCAPE:
                //set portrait
                ScreenshotImageMobile.GetComponent<RectTransform>().sizeDelta = ScreenshotImageTablet.GetComponent<RectTransform>().sizeDelta = new Vector2(Util.ScreenHeight, Util.ScreenHeight * Util.ScreenHeight / Util.ScreenWidth);
                break;
        }
    }*/
    public IEnumerator SaveScreenshotRoutine(Action<Texture2D> result)
    {
        yield return new WaitForEndOfFrame();
        var tex = new Texture2D((int)Util.ScreenWidth, (int)Util.ScreenHeight, TextureFormat.RGB24, false);
        tex.ReadPixels(new Rect(0, 0, (int)Util.ScreenWidth, (int)Util.ScreenHeight), 0, 0);
        tex.Apply();
        result(tex);
    }

    public void EMail()
    {
        PopupManager.Instance.OpenPopup(PopupType.MAIL,
            new Dictionary<string, object>() { { "filename", FileName } }
            /*, new System.Collections.Generic.Dictionary<string, object> (){ { "ScreenshotImageMobile", ScreenshotImageMobile } }*/);
    }

    string _fileName;
    [HideInInspector]
    public string FileName
    {
        get
        {
            return _fileName;
        }
        set
        {
            _fileName = value;
            if (!_fileName.EndsWith(".png"))
                _fileName += ".png";
        }

    }
    void SaveButtonClicked()
    {
        PopupManager.Instance.OpenPopup(PopupType.SAVEIMAGE, new Dictionary<string, object>() { { "filename", FileName } });
    }

    public void SaveImageInGalery(string filename, bool screenshotTicked, bool mapTicked)
    {
        if (screenshotTicked && imageBytes != null)
        {
            NativeGallery.SaveImageToGallery(imageBytes, "ARScreenshots", filename);
        }
        if (mapTicked && Main.Instance.mapLocation.mapBytes != null)
        {
            NativeGallery.SaveImageToGallery(Main.Instance.mapLocation.mapBytes, "ARScreenshots", "Map" + filename);
        }
    }

    public override void OpenPopup(Dictionary<string, object> data)
    {
        Root.SetActive(false);
        StartCoroutine(CaptureScreenshot());
    }

    public override void ClosePopup()
    {
        Debug.Log("Mailing: DeclineSavingScreenshot");
        //OrientationHandler.Instance.UnsubscribeOrientationChanged(OrientationChanged);
    }


    #region animations
    public Transform TopRoot;
    public Transform BottomRoot;

    public Transform StartAnimationTop;
    public Transform EndAnimationTop;

    public Transform StartAnimationBottom;
    public Transform EndAnimationBottom;

    protected override IEnumerator CustomStartAnimation()
    {
        if (!TopRoot || !BottomRoot) yield break;
        float startTime = Time.time;
        while (startTime + STARTANIMDURATION > Time.time)
        {
            float t = (Time.time - startTime) / STARTANIMDURATION;
            TopRoot.position = new Vector3(TopRoot.position.x, Mathf.SmoothStep(StartAnimationTop.position.y, EndAnimationTop.position.y, (Time.time - startTime) / STARTANIMDURATION), TopRoot.position.z);
            BottomRoot.position = new Vector3(BottomRoot.position.x, Mathf.SmoothStep(StartAnimationBottom.position.y, EndAnimationBottom.position.y, (Time.time - startTime) / STARTANIMDURATION), BottomRoot.position.z);
            yield return null;
        }
        TopRoot.position = EndAnimationTop.position;
        BottomRoot.position = EndAnimationBottom.position;
    }
    protected override IEnumerator CustomEndAnimation()
    {
        if (!TopRoot || !BottomRoot) yield break;
        float startTime = Time.time;
        while (startTime + STARTANIMDURATION > Time.time)
        {
            float t = (Time.time - startTime) / STARTANIMDURATION;
            TopRoot.position = new Vector3(TopRoot.position.x, Mathf.SmoothStep(EndAnimationTop.position.y, StartAnimationTop.position.y, (Time.time - startTime) / STARTANIMDURATION), TopRoot.position.z);
            BottomRoot.position = new Vector3(BottomRoot.position.x, Mathf.SmoothStep(EndAnimationBottom.position.y, StartAnimationBottom.position.y, (Time.time - startTime) / STARTANIMDURATION), BottomRoot.position.z);
            yield return null;
        }
        TopRoot.position = StartAnimationTop.position;
        BottomRoot.position = StartAnimationBottom.position;
    }
    #endregion
}


