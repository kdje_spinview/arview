﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public enum AxisType
{
    X,
    Y,
    Z
}
public class Axis : MonoBehaviour
{
    public Text MeasureText;
    public AxisType AxisType;
    [HideInInspector] public GameObject[] EdgePoints;
    public GameObject Root;
    public LineRenderer Line;
    public UILineRenderer UiLine;


    GameObject _placeholder = null;
    Measurements _measurements;

    Camera arCamera;

    private void Awake()
    {
        _measurements = Main.Instance._measurements;


        arCamera =
#if UNITY_WSA
            Camera.main
#else
            InteractionController.Instance.camera
#endif
            ;
    }

    float _size = 0f;
    void SetText()
    {
        //TODO: check if it is ...transform.position
        //TODO for windows: consider adding markerSize multiplyer
        float newSize = Vector3.Distance(EdgePoints[0].transform.position, EdgePoints[1].transform.position);
        if (_size != newSize)
        {
            _size = newSize;
            
            MeasureText.text = String.Format("{0:0.##}", _size
#if UNITY_WSA
                * 19f
#else
                * 100f
#endif
                ) + "cm";
        }
    }

    public void SetPlaceHolder(GameObject placeholder)
    {
        _placeholder = placeholder;
    }
    
    bool _visible = true;
    private void Update()
    {
        bool newVisible = IsVisible();
        if (_visible != newVisible)
        {
            _visible = newVisible;
            Root.SetActive(_visible);
        }
        if(_visible && _placeholder != null)
        {
            Vector3 axisCenter = arCamera.WorldToScreenPoint(_placeholder.transform.position);
            transform.position = axisCenter;
            SetText();

            Vector3 edge1 = arCamera.WorldToScreenPoint(EdgePoints[0].transform.position);
            Vector3 edge2 = arCamera.WorldToScreenPoint(EdgePoints[1].transform.position);

            //Line.SetPosition(0, EdgePoints[0].transform.position);
            //Line.SetPosition(1, EdgePoints[1].transform.position);
            UiLine.Points[0] = (edge1 - axisCenter) * 2f ;
            UiLine.Points[1] = (edge2 - axisCenter) * 2f ;
            
            UiLine.gameObject.SetActive(false);
            UiLine.gameObject.SetActive(true);
            
            //
            //            float axisWidth = Vector3.Distance(edge1, edge2);
            //            LineRect.sizeDelta = new Vector2(axisWidth, 5f);
            //
            //            float firstHalfAxisWidth = Vector3.Distance(edge1, axisCenter);
            //            LineRect.anchoredPosition = new Vector2(firstHalfAxisWidth - axisWidth / 2, 0f);
            //
            //#if !UNITY_EDITOR && UNITY_ANDROID
            //            LineRect.transform.eulerAngles = new Vector3(0f, 0f, GetZAngle(edge1, edge2));
            //#endif
            //Debug.Log("firstHalfAxisWidth - axisWidth / 2 = " + (firstHalfAxisWidth - axisWidth / 2));
        }
    }
    float GetZAngle(Vector3 point1, Vector3 point2)
    {
        //Debug.Log("atan = " = Mathf.Atan2(point1.x - point2.x, point1.y - point2.y));
        return -(Mathf.Atan((point1.x - point2.x) / (point1.y - point2.y)) * (180f / (float)Math.PI) + 90f);
    }

    public RectTransform LineRect;

    public static bool is3dModel;
    bool IsVisible()
    {
        //Debug.Log("IsVisible start");
        try
        {
#if UNITY_WSA
            if (!MainTrackableEventHandler.Tracking) return false;
#endif
            if (PopupManager.Instance.AnyPopupOpened() && !ScreenshotManager.Capturing) return false;

            //Debug.Log("IsVisible 2");
            //if (!is3dModel) return true;

            RaycastHit hitInfo1 = new RaycastHit();
            RaycastHit hitInfo2 = new RaycastHit();
            if (EdgePoints == null || EdgePoints.Length != 2) return false;

            if (!Physics.Linecast(arCamera.transform.position, EdgePoints[0].transform.position, out hitInfo1)
                &&
                !Physics.Linecast(arCamera.transform.position, EdgePoints[1].transform.position, out hitInfo2))
                return true;

            //Debug.Log("IsVisible 3");
            bool plane = false;
            if(!plane && hitInfo1.transform != null && hitInfo1.transform.gameObject != null)
                plane = hitInfo1.transform.gameObject.name.Contains("Plane");

            if (!plane && hitInfo2.transform != null && hitInfo2.transform.gameObject != null)
                plane = hitInfo2.transform.gameObject.name.Contains("Plane");

            if (hitInfo1.collider is BoxCollider || hitInfo2.collider is BoxCollider)
            {
                //Debug.Log("IsVisible hitInfo.collider is BoxCollider " + (hitInfo1.collider is BoxCollider) + (hitInfo2.collider is BoxCollider));
                plane = true;
            }
            //Debug.Log("IsVisible 3 plane = " + plane);
            /*test
             * if (hitInfo1.transform == null) Debug.Log("hitInfo1.transform = null");
            else if (hitInfo1.transform.gameObject == null) Debug.Log("hitInfo1.transform.gameObject = null");
            else Debug.Log("hitInfo1 = " + hitInfo1.transform.gameObject.name);

            if (hitInfo2.transform == null) Debug.Log("hitInfo2.transform = null");
            else if (hitInfo2.transform.gameObject == null) Debug.Log("hitInfo2.transform.gameObject = null");
            else Debug.Log("hitInfo2 = " + hitInfo2.transform.gameObject.name);*/

            return plane;
        }
        catch(Exception e)
        {
            return false;
        }
    }
}
