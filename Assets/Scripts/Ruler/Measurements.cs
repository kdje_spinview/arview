﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Measurements : MonoBehaviour
{
    public GameObject Root;

    public Axis AxisX;
    public Axis AxisY;
    public Axis AxisZ;

    BoxCollider _boxCollider;
    GameObject[] _mainPoints;
    
    public void Activate(bool activate)
    {
        Root.SetActive(activate);
    }

    public void SetMainPoints(GameObject[] points)
    {
        _mainPoints = points;

        AxisX.EdgePoints = new GameObject[2];
        AxisX.EdgePoints[0] = points[0];
        AxisX.EdgePoints[1] = points[1];

        AxisY.EdgePoints = new GameObject[2];
        AxisY.EdgePoints[0] = points[0];
        AxisY.EdgePoints[1] = points[2];

        AxisZ.EdgePoints = new GameObject[2];
        AxisZ.EdgePoints[0] = points[0];
        AxisZ.EdgePoints[1] = points[3];
    }
    public void SetPlaceHolders(GameObject xPlaceholder, GameObject yPlaceholder, GameObject zPlaceholder)
    {
        AxisX.SetPlaceHolder(xPlaceholder);
        AxisY.SetPlaceHolder(yPlaceholder);
        AxisZ.SetPlaceHolder(zPlaceholder);
    }
}
