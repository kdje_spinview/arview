﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ruler2d : MonoBehaviour
{
    MeshRenderer _mesh;
    GameObject _xAxisPlaceHolder;
    GameObject _yAxisPlaceHolder;


    void Start()
    {
        Debug.Log("Ruler2d bstart");
        _mesh = GetComponent<MeshRenderer>();

        AddColliderNodes();
    }
    BoxCollider _collider;
    void AddColliderNodes()
    {
        Debug.Log("Ruler2d AddColliderNodes");
        GameObject rulerRoot = new GameObject();
        rulerRoot.name = "RulerRoot";
        rulerRoot.transform.SetParent(transform);
        rulerRoot.transform.localPosition = Vector3.zero;
        rulerRoot.transform.localScale = Vector3.one;
        rulerRoot.transform.localRotation = Quaternion.Euler(Vector3.zero);

        Debug.Log("Ruler2d AddColliderNodes2");
        GameObject[] nodes = new GameObject[4];
        for(int i = 0; i < 4; i++)
        {
            nodes[i] = new GameObject();
            nodes[i].transform.SetParent(rulerRoot.transform);
            nodes[i].name = "main point " + i;
            nodes[i].transform.localPosition = new Vector3(
                ((i & (1 << 0)) != 0 ? 1f : -1f) * 0.501f,
                ((i & (1 << 1)) != 0 ? 1f : -1f) * 0.501f,
                0f
                );
        }
        Debug.Log("Ruler2d AddColliderNodes3");
        Main.Instance._measurements2d.SetMainPoints(nodes);


        Debug.Log("Ruler2d AddColliderNodes4");
        _collider = gameObject./*transform.GetChild(0).gameObject.*/AddComponent<BoxCollider>();

        Debug.Log("Ruler2d AddColliderNodes5");
        _xAxisPlaceHolder = new GameObject();
        _xAxisPlaceHolder.transform.SetParent(rulerRoot.transform);
        _xAxisPlaceHolder.transform.localPosition = (nodes[0].transform.localPosition + nodes[2].transform.localPosition) / 2;
        _xAxisPlaceHolder.name = "xPlaceHolder";

        _yAxisPlaceHolder = new GameObject();
        _yAxisPlaceHolder.transform.SetParent(rulerRoot.transform);
        _yAxisPlaceHolder.transform.localPosition = (nodes[2].transform.localPosition + nodes[3].transform.localPosition) / 2;
        _yAxisPlaceHolder.name = "yPlaceHolder";

        Debug.Log("Ruler2d AddColliderNodes6");
        Main.Instance._measurements2d.SetPlaceHolders(_xAxisPlaceHolder, _yAxisPlaceHolder);

        Debug.Log("Ruler2d end");
    }
}
