﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ruler : MonoBehaviour
{
    BoxCollider _collider;

    void Start()
    {
        AddBoxCollider();

        CreateMeasurements();
    }

    void AddBoxCollider()
    {
        Quaternion savedRotation = transform.rotation;
        transform.rotation = Quaternion.identity;

        MeshRenderer[] meshRenderers = transform.GetComponentsInChildren<MeshRenderer>();
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        bool hasBounds = false;
        foreach (MeshRenderer mR in meshRenderers)
        {
            if (mR != null)
            {
                if (hasBounds)
                {
                    bounds.Encapsulate(mR.bounds);
                }
                else
                {
                    bounds = mR.bounds;
                    hasBounds = true;
                }
            }
        }
        _collider = gameObject.AddComponent<BoxCollider>();
        _collider.size = bounds.size;
        _collider.center = bounds.center - transform.position;

        transform.rotation = savedRotation;
    }

    GameObject _xAxisPlaceHolder;
    GameObject _yAxisPlaceHolder;
    GameObject _zAxisPlaceHolder;

    void CreateMeasurements()
    {
        float scale = Mathf.Abs(1f / InteractionController.Instance.objectToPlace.transform.localScale.x);
        //get closest point to camera
        
        //Vector3 centerPoint = InteractionController.Instance.objectToPlace.transform.position + _collider.center;// * scale;
        GameObject rulerRoot = new GameObject();
        rulerRoot.name = "RulerRoot";
        rulerRoot.transform.SetParent(transform);
        rulerRoot.transform.localPosition = _collider.center;// closestPoint;
        rulerRoot.transform.localScale = Vector3.one;
        rulerRoot.transform.localRotation = Quaternion.Euler(Vector3.zero);


        rulerRoot.transform.localScale = Vector3.one;
        rulerRoot.transform.localRotation = Quaternion.Euler(Vector3.zero);


        Camera arCamera =
#if UNITY_WSA
            Camera.main
#else
            InteractionController.Instance.camera
#endif
            ;

        GameObject[] closestPointGOs = Util.GetClosestBoxColliderNodes(rulerRoot.transform, _collider, arCamera.transform.position, scale);
        
        for(int i = 0; i < 4; i++)
        {
            closestPointGOs[i].name += i + "th closest";
        }

        _xAxisPlaceHolder = new GameObject();
        _xAxisPlaceHolder.transform.SetParent(rulerRoot.transform);
        _xAxisPlaceHolder.transform.localPosition = (closestPointGOs[0].transform.localPosition + closestPointGOs[1].transform.localPosition) / 2;

        _yAxisPlaceHolder = new GameObject();
        _yAxisPlaceHolder.transform.SetParent(rulerRoot.transform);
        _yAxisPlaceHolder.transform.localPosition = (closestPointGOs[0].transform.localPosition + closestPointGOs[2].transform.localPosition) / 2;

        _zAxisPlaceHolder = new GameObject();
        _zAxisPlaceHolder.transform.SetParent(rulerRoot.transform);
        _zAxisPlaceHolder.transform.localPosition = (closestPointGOs[0].transform.localPosition + closestPointGOs[3].transform.localPosition) / 2;

        Main.Instance._measurements.SetMainPoints(closestPointGOs);
        Main.Instance._measurements.SetPlaceHolders(_xAxisPlaceHolder, _yAxisPlaceHolder, _zAxisPlaceHolder);
    }
}
