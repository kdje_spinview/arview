﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ProductPopup : Popup
{
    public RectTransform XButtonRT;

    public GameObject CompanyPrefab;
    public GameObject ModelPrefab;

    public Transform CompaniesRoot;
    public Transform ModelsRoot;

    public ScrollRect CompaniesScroll;
    public RectTransform CompaniesScrollTransform;
    
    public RectTransform CompaniesRectTransform;
    public RectTransform ModelsRectTransform;

    public Text StatusMessageText;
    public Button ReloadButton;

    #region data
    Dictionary<string, List<string>> _foldersAndModels;
    Dictionary<string, string> _models;
    Dictionary<string, string> _images;
    public static Dictionary<string, Sprite> _imageSprites = new Dictionary<string, Sprite>();
    #endregion
    public static string _currentCompany = "";
    private void Awake()
    {
        //SelectCompany(_currentCompany);
        StatusMessageText.text = LocalizationController.Instance.GetTranslation(Settings.LOADING);
        ReloadButton.onClick.AddListener(Main.Instance.ReloadProductPopup);
    }
    public void SetCompaniesAndModels()
    {
        Dictionary<string, ModelData> modelData = new Dictionary<string, ModelData>();
        if (Settings.PlatformModels.Count > 0)
        {
            modelData = Settings.PlatformModels;
        }
        else
        {
            modelData = Settings.StoredModels;
        }

        if (modelData == null || modelData.Count == 0)
        {
            return;
        }

        StatusMessageText.text = LocalizationController.Instance.GetTranslation(Settings.LOADING);
        //ReloadButton.gameObject.SetActive(true);
        if (modelData.Count == 0)
        {
            StatusMessageText.text = LocalizationController.Instance.GetTranslation(Settings.NO_MODELS_AVAILABLE);
            //ReloadButton.gameObject.SetActive(true);
            return;
        }
        try
        {
            _models = new Dictionary<string, string>();
            _images = new Dictionary<string, string>();
            foreach (var model in modelData)
            {
                if (!_models.ContainsKey(model.Key)) _models.Add(model.Key, model.Value.folderName);
                if (!_images.ContainsKey(model.Key)) _images.Add(model.Key, model.Value.imageURL);
            }
            _foldersAndModels = new Dictionary<string, List<string>>();
            foreach (KeyValuePair<string, string> kvModel in _models)
                if (_foldersAndModels.ContainsKey(kvModel.Value))
                    _foldersAndModels[kvModel.Value].Add(kvModel.Key);
                else
                    _foldersAndModels.Add(kvModel.Value, new List<string> { kvModel.Key });

            string firstCompany = string.Empty;
            foreach (KeyValuePair<string, List<string>> folderKv in _foldersAndModels)
            {
                if (string.IsNullOrEmpty(folderKv.Key)) continue;
                CreateCompany(folderKv.Key);
                if (string.IsNullOrEmpty(firstCompany)) firstCompany = folderKv.Key;
            }

            if (_currentCompany.Equals(""))
            {
                SelectCompany(firstCompany, true);
            }
            else
            {
                SelectCompany(_currentCompany, true);
            }

            StatusMessageText.gameObject.SetActive(false);
            ReloadButton.gameObject.SetActive(false);
        }
        catch (Exception e)
        {
            StatusMessageText.text = LocalizationController.Instance.GetTranslation(Settings.SOMETHING_WENT_WRONG);
            //ReloadButton.gameObject.SetActive(true);
            //Main.Instance.Notify("something_went_wrong");
        }
    }
    void CreateCompany(string folderName)
    {
        ProductCompany company = Instantiate(CompanyPrefab, CompaniesRoot).GetComponent<ProductCompany>();
        company.gameObject.name = folderName;
        company.CompanyNameText.text = folderName;
    }
    public void SelectCompany(string folderName, bool start = false)
    {
        //if (folderName.Equals("")) return;
        if (_currentCompany == folderName && !start) return;
        for (int i = ModelsRoot.transform.childCount - 1; i >= 0; i--) Destroy(ModelsRoot.GetChild(i).gameObject);
        _currentCompany = folderName;
        //deactivate companies
        foreach (Transform company in CompaniesRoot)
            company.GetComponent<ProductCompany>().SelectedFlag.SetActive(false);
        //destroy models
        for(int i = ModelsRoot.childCount - 1; i >= 0; i--)
            Destroy(ModelsRoot.GetChild(i));
        //activate company
        ProductCompany selectedCompany = CompaniesRoot.Find(folderName).GetComponent<ProductCompany>();
        selectedCompany.SelectedFlag.SetActive(true);
        //create new models
        List<string> models = _foldersAndModels[folderName];
        if (models == null) return;
        foreach (string model in models)
            CreateModel(model);

        CenterToItem(selectedCompany.GetComponent<RectTransform>());
    }
    void CreateModel(string modelName)
    {
        if (string.IsNullOrEmpty(modelName)) return;
        ProductModel model = Instantiate(ModelPrefab, ModelsRoot).GetComponent<ProductModel>();
        model.ModelName.text = modelName.Length > 17 ? modelName.Substring(0, 15) + "..." : modelName;
        model._modelName = modelName;
        if (_models.ContainsKey(modelName))
            model.CompanyName.text = _models[modelName];
        if (_images.ContainsKey(modelName))
        {
            if(_imageSprites.ContainsKey(_images[modelName]))
                model.ModelImage.sprite = _imageSprites[_images[modelName]];
            else
                StartCoroutine(model.DownloadImage(_images[modelName]));
        }
    }

    public void CenterToItem(RectTransform obj)
    {
        float normalizePosition = CompaniesScrollTransform.anchorMin.y - obj.anchoredPosition.y;
        normalizePosition += (float)obj.transform.GetSiblingIndex() / (float)CompaniesScroll.content.transform.childCount;
        normalizePosition /= 1000f;
        normalizePosition = Mathf.Clamp01(1 - normalizePosition);
        CompaniesScroll.verticalNormalizedPosition = normalizePosition + 45f;
        //Debug.Log(normalizePosition);
    }

    public static bool _productplaced = false;
    public static bool _tutorialWatched = false;
    public void SelectModel(string modelName)
    {
        //TODO: Show white rect in world
        if (Main.Instance.ModelNotAddedButton.gameObject.activeSelf) Main.Instance.ModelNotAddedButton.gameObject.SetActive(false);
        PopupManager.Instance.ClosePopup();
        Main.Instance.guiController.RulerButton.gameObject.SetActive(true);
        
        POSFileManager.Instance.SelectPOS(modelName);
#if UNITY_WSA
        Main.Instance.ActivateRulerClicked(Prefs.RulerAcive);
#else
        Main.Instance._measurements.Activate(false);
        Main.Instance._measurements2d.Activate(false);
#endif    
    }
    #region device_type
    public GameObject Root;
    const float widthFactor = 1080f;
    void SetDevice(DeviceType deviceType)
    {
        
        Orientation orientation = Util.GetOrientation();
        switch (deviceType)
        {
            case DeviceType.NONE:
                Root.SetActive(false);
                break;
            case DeviceType.MOBILE:
                Root.SetActive(true);
                //Root.transform.localScale = Vector3.one * (Util.ScreenWidth / (orientation == Orientation.PORTRAIT ? 1080f : 1920f));// * (80f / 100f);
                //posParam = 1f / Root.transform.localScale.x;
                SetXButtonPosition();
                break;
            case DeviceType.TABLET:
                Root.SetActive(true);
                Root.transform.localScale = Vector3.one * (Util.ScreenWidth / (orientation == Orientation.PORTRAIT ? 1080f : 1920f)) / 2;// * (80f / 100f);
                posParam = 1f / Root.transform.localScale.x;
                SetXButtonPosition();
                break;
        }
        CompaniesRectTransform.sizeDelta = new Vector2(Main.Instance.canvasRectTransform.rect.width * posParam, CompaniesRectTransform.sizeDelta.y);
        ModelsRectTransform.sizeDelta = new Vector2(Main.Instance.canvasRectTransform.rect.width * posParam, ModelsRectTransform.sizeDelta.y);

    }
    void SetHeight(RectTransform root, float percent)
    {
        root.sizeDelta = new Vector2(root.sizeDelta.x, (Util.ScreenWidth < Util.ScreenHeight ? Util.ScreenWidth : Util.ScreenHeight) * percent / 100f);
    }
#endregion

#region orientation
    private void Start()
    {
        _currentOrientation = Orientation.PORTRAIT;
        SetCompaniesAndModels();
        StartCoroutine(CheckOrientation());
    }
    Orientation _currentOrientation;
    IEnumerator CheckOrientation()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.25f);

            Orientation newOrientation = Util.GetOrientation();
            if (_currentOrientation != newOrientation)
            {
                OrientationChanged(newOrientation);
                _currentOrientation = newOrientation;
            }
        }
    }
    public RectTransform XButtonRectTransform;
    void OrientationChanged(Orientation newOrientation)
    {
        float screenHeight = Util.ScreenHeight;
        float screenWidth = Util.ScreenWidth;
        switch (newOrientation)
        {
            case Orientation.PORTRAIT:
                XButtonRectTransform.anchoredPosition = new Vector2(screenWidth - 100f, 0f);
                break;
            case Orientation.LANDSCAPE:
                XButtonRectTransform.anchoredPosition = new Vector2(screenHeight - 100f, 0f);
                break;
        }
        CompaniesRectTransform.sizeDelta = new Vector2(Main.Instance.canvasRectTransform.rect.width * posParam * posParam, CompaniesRectTransform.sizeDelta.y);
        ModelsRectTransform.sizeDelta = new Vector2(Main.Instance.canvasRectTransform.rect.width * posParam * posParam, ModelsRectTransform.sizeDelta.y);
        SetXButtonPosition();
    }
    float posParam = 1f;
    void SetXButtonPosition()
    {
        //XButtonRT.anchoredPosition = new Vector2(Util.ScreenWidth * posParam - 100f, 0f);
        XButtonRT.anchoredPosition = new Vector2(Main.Instance.canvasRectTransform.rect.width * posParam - 100f, 0f);
    }

    public override void OpenPopup(Dictionary<string, object> data)
    {
        SetDevice(Util.GetDevice());
    }

    public override void ClosePopup()
    {
    }
#endregion
}
