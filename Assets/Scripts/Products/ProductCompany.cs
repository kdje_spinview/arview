﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ProductCompany : MonoBehaviour
{
    public Text CompanyNameText;
    public GameObject SelectedFlag;
    public Button SelectCompanyButton;
    
    private void Awake()
    {
        SelectCompanyButton.onClick.AddListener(SelectCompany);
    }
    void SelectCompany()
    {
        var productPopup = (ProductPopup)PopupManager.Instance.GetPopup(PopupType.PRODUCT);
        if (productPopup != null) productPopup.SelectCompany(CompanyNameText.text);
    }
    private void OnEnable()
    {
        StartCoroutine(SetFlagSize());
    }
    IEnumerator SetFlagSize()
    {
        yield return new WaitForEndOfFrame();
        RectTransform SelectedFlagRectTransform = SelectedFlag.GetComponent<RectTransform>();
        SelectedFlagRectTransform.sizeDelta = new Vector2(CompanyNameText.GetComponent<RectTransform>().sizeDelta.x, SelectedFlagRectTransform.sizeDelta.y);
    }
}
