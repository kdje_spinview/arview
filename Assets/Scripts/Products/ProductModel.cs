﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ProductModel : MonoBehaviour
{
    public Button SelectModelButton;
    public Text ModelName;
    public Text CompanyName;
    public Image ModelImage;

    public string _modelName;
    string _imageUrl;


    private void Awake()
    {
        SelectModelButton.onClick.AddListener(SelectModel);
    }

    void SelectModel()
    {
        var productPopup = (ProductPopup)PopupManager.Instance.GetPopup(PopupType.PRODUCT);
        if(productPopup != null) productPopup.SelectModel(_modelName);
    }
    public IEnumerator DownloadImage(string imageUrl)
    {
        _imageUrl = imageUrl;
        //WWW www = new WWW("https://api.spinviewglobal.com/api/v1/images/" + _imageUrl + "?size=100x100");

        int token = WebRequest.GetNewToken();
        yield return WebRequest.Get(token, "https://api.spinviewglobal.com/api/v1/images/" + _imageUrl + "?size=100x100", true);

        //yield return www;
        Texture2D texture2d = WebRequest.TextureData(token);
        //if (www.texture != null)
        if (!WebRequest.Error(token) && texture2d != null)
        {
            ModelImage.sprite = Sprite.Create(texture2d, new Rect(0, 0, texture2d.width, texture2d.height), new Vector2(0, 0));

            if(!ProductPopup._imageSprites.ContainsKey(imageUrl))
                ProductPopup._imageSprites.Add(imageUrl, ModelImage.sprite);

            WebRequest.RemoveData(token);
        }
    }
}
