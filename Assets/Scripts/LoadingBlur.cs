﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingBlur : MonoBehaviour
{
    public LocalizationElement LocalizationElement;

    public void Activate(string loadingMessage = "")
    {
        LocalizationElement.Key = string.IsNullOrEmpty(loadingMessage) ? "loading" : loadingMessage;
        LocalizationElement.SetText();

        if (Util.GetDevice() == DeviceType.TABLET)
            LocalizationElement.transform.localScale = Vector3.one * 0.5f;

        gameObject.SetActive(true);
    }
    public void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
