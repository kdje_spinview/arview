﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class LoadingBar : MonoBehaviour
{
    public GameObject LoadingRoot;
    public Slider Slider;
    public Text PercentText;

    float _currentProgress;
    float CurrentProgress
    {
        get
        {
            return _currentProgress;
        }
        set
        {
            _currentProgress = value;
            if (_currentProgress < 0) _currentProgress = 0;
            if (_currentProgress > 1) _currentProgress = 1;

            //Debug.Log("Loading: set _currentProgress = " + _currentProgress);
            //PercentText.text = ((int)(_currentProgress * 100f)).ToString();
            Slider.value = _currentProgress;
        }
    }
    public void StartLoading(UnityWebRequest www)
    {
        Debug.Log("Loading: StartLoading");
        LoadingRoot.SetActive(true);

        StartCoroutine(FixLoadingBar(www));
    }
    IEnumerator FixLoadingBar(UnityWebRequest www)
    {
        while (www.downloadProgress < 1f)
        {
            CurrentProgress = www.downloadProgress;
            yield return null;
        }

        CurrentProgress = 1f;
        yield return new WaitForEndOfFrame();

        EndLoading();
    }
    public void EndLoading()
    {
        Debug.Log("Loading: EndLoading");
        LoadingRoot.SetActive(false);
    }
}
