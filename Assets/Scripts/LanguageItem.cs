﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageItem : MonoBehaviour
{
    public Image image;
    public Text text;
    public Sprite checkSprite, uncheckSprite;
    public LocalizationElement _localizationElement;

    public static SettingsPopup _settings;


    Language _language;
    public Language Language
    {
        set
        {
            _language = value;
            _localizationElement.Key = _language.ToString().ToLower();
            _localizationElement.SetText();
            //text.text = LocalizationController.Instance.GetTranslation(_language.ToString().ToLower());
        }
    }
    public void Check(Language currentLang)
    {
        image.sprite = _language == currentLang ? checkSprite : uncheckSprite;
    }
    public void CheckButton()
    {
        Prefs.Language = _language;
        LocalizationController.Instance.CurrentLanguage = _language;
        _settings.Check(_language);
    }
}
