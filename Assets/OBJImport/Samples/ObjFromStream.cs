﻿using Dummiesman;
using System.IO;
using System.Text;
using UnityEngine;

public class ObjFromStream : MonoBehaviour {
	void Start () {
        //make www
        var www = new WWW("http://arcontent.spinview.io/MerckAR/treasure_chest_android.obj");
        while (!www.isDone)
        {
#if UNITY_WSA && !UNITY_EDITOR
            System.Threading.Tasks.Task.Delay(1).Wait();
#else
            System.Threading.Thread.Sleep(1);
#endif
        }
        
        //create stream and load
        var textStream = new MemoryStream(Encoding.UTF8.GetBytes(www.text));
        var loadedObj = new OBJLoader().Load(textStream);
	}
}
